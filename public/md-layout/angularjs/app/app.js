'use strict';

/**
 * @ngdoc overview
 * @name newappApp
 * @description
 * # newappApp
 *
 * Main module of the application.
 */
var MakeApp = angular
  .module('newApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
	'ngTable',
	'ngMessages',
	'ui.select2',
	'uiGmapgoogle-maps',
  ])
  .config(function ($routeProvider) {
      $routeProvider
		 //roles
        .when('/roles', {
            templateUrl: 'md-layout/angularjs/app/roles/roles.html',
            controller: 'rolesCtrl'
        })

        .when('/agreements/customers', {
            templateUrl: 'md-layout/angularjs/app/agreements/customers/agreementscustomers.html',
            controller: 'agreementcustomerssCtrl'
        })
		
        .otherwise({
            redirectTo: '/'
        }),
		function(uiGmapGoogleMapApiProvider) {
              uiGmapGoogleMapApiProvider.configure({
                  key: 'AIzaSyDUTFqU5rkkH0T0WJLqIvLovm0O5ftjDJ4',
                  v: '3.20', //defaults to latest 3.X anyhow
                  libraries: 'places' // Required for SearchBox.
              });
        }
  });


// Route State Load Spinner(used on page or content load)
MakeApp.directive('ngSpinnerLoader', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default
                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });
                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$routeChangeSuccess', function() {
                    setTimeout(function(){
                        element.addClass('hide'); // hide spinner bar
                    },500);
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);   
                });
            }
        };
    }
])