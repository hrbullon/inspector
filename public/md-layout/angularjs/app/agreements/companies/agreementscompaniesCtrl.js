angular.module('newApp')
/****Routing****/
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/agreements/companies/create', {
            templateUrl: 'md-layout/angularjs/app/agreements/companies/form.html',
            controller: 'createAgreementsCompaniesCtrl'
        });
        $routeProvider.when('/agreements/companies/edit/:id', {
            templateUrl: 'md-layout/angularjs/app/agreements/companies/form.html',
            controller: 'editAgreementsCompaniesCtrl'
        });
    }])
/****Controllers****/
    .controller('agreementscompaniesCtrl', ["$scope", "ngTableParams", "AgreementsCompanies", "$http", "$resource", "$route", "$routeParams", "applicationService",
        function ($scope, ngTableParams, AgreementsCompanies, $http, $resource, $route, $routeParams, applicationService) {

            $scope.settings = {module: "Contratos de Empresas", action: "agreements/companies" ,title: "Listado", icon: "mdi-content-add"};
            $scope.agreementsSelected = [];

            var buscar = "";

            $scope.filter = function () {
                if ($scope.buscar !== undefined || $scope.buscar == "") {
                    buscar = $scope.buscar;

                    $scope.tableParams.reload();
                } else {
                    $scope.tableParams.reload();
                }
            }

            $scope.tableParams = new ngTableParams({
                    page: 1,          // primera página a mostrar
                    count: 30,
                    sorting: {id: "desc"}
                },
                {
                    getData: function ($defer, params) {
                        var url = 'agreementscompanies?search=' + buscar + '&page=' + params.page() + '&per_page=' + params.count() + '&order=' + params.orderBy();
                        $http({url: url, method: "get"}).then(function successCallback(response) {
                            $scope.agreementsSelected = [];
                            $scope.start = response.data.from;
                            $scope.end = response.data.to;
                            $scope.total = response.data.total;
                            $scope.total = response.data.total;//total de registros
                            $scope.inicio = response.data.from;//Desde donde esta mostrando
                            $scope.fin = response.data.to;//Hasta donde esta mostrado
                            $scope.tableParams.total($scope.total);//Total de registros
                            return $defer.resolve(response.data.data)
                        });
                    },
                });

            $scope.annullar = function (id) {
                if (confirm("¿Está seguro de  borrar este elemento?")) {
                    AgreementsCompanies.delete({id: id}).$promise.then(function (data) {
                        if (data.code == 200) {
                            applicationService.generateNoty("success", data.msg);
                            $scope.tableParams.reload();
                        } else {
                            applicationService.generateNoty("danger", data.msg);
                        }
                    })
                }
            }

            $scope.viewAgreement = function(id)
            {
                AgreementsCompanies.get({id: id}, function (response) {
                    $scope.company = response.data.company;
                    $scope.agreement = response.data.agreement;
                    $scope.packages = response.data.packages;
                });

                $("#btnmodal").trigger("click");
            }

            $scope.selecteRow = function (row, id, $index) {
                if($("#"+row).attr("class") == "ng-scope active"){
                    $("#"+row).removeClass("active");
                    $scope.agreementsSelected.splice($index, 1);
                }else{
                    $("#"+row).addClass("active");
                    $scope.agreementsSelected.push({id:id});
                }
            }

        }])

    .controller('createAgreementsCompaniesCtrl', ["$scope", "AgreementsCompanies", "applicationService", "$http", function ($scope, AgreementsCompanies, applicationService, $http) {

        $scope.settings = {module: "Contratos de Empresas", action: "agreementsCompanies", title:'Nuevo', icon: "mdi-content-add", botonclass: "btn-success"};

        $scope.base = 0;

            $scope.arrayAsync = {
                query: function (query) {

                    if (query.term == "") {
                        query.callback({results: [{id: 0, text: "Ingrese una palabra a buscar"}]});
                    } else {
                        $http({
                            url: "companies/autocomplete",
                            method: "post",
                            data: {search: query.term}
                        }).then(function successCallback(response) {
                            query.callback({results: response.data});
                        });
                    }
                },
            };

            $http({
                url: "status/getcombobox",
                method: "post",
                data: {"source": 14}
            }).then(function successCallback(response) {
                $scope.selectedFrecuency = response.data[0].id;
                $scope.frecuencies = response.data;

            });

            $http({
                url: "status/getcombobox",
                method: "post",
                data: {"source": 15}
            }).then(function successCallback(response) {
                $scope.type_sales = response.data;
            });

            $scope.agreement = {
                company: "", frecuency: "", tope_n_customers: "", seller: "", date: "", type_sale: "", m_status: "1",
                /*****menus, nitficications y otros servicios******/
                activations: "", inspections: "", installations: "", users: "",
                /*****modelos para llenar los combobox************/
                sms: "", mail: "", push: "", device: "", wa: "", st: "",
            };

            ($scope.agreement.m_status == 0) ? $("#mstatus").removeAttr("checked") : $("#mstatus").attr("checked");

            $("#date").datepicker({
                prevText: '<i class="fa fa-angle-left"></i>',
                nextText: '<i class="fa fa-angle-right"></i>',
                clearText: 'Eliminar', clearStatus: '',
                closeText: 'Cerrar', closeStatus: 'Cerrar sin cambiar',
                prevStatus: 'Ver mes anterior',
                nextStatus: 'Ver mes siguiente',
                currentText: 'Actual', currentStatus: 'Ver mes actual',
                monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
                monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
                weekHeader: 'Sm', weekStatus: '',
                dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
                dayNamesMin: ['D','L','M','X','J','V','S'],
                dateFormat: 'yy-mm-dd', firstDay: 0, 
                initStatus: 'Elegir una fecha', isRTL: false 
            });

            $scope.clearInfo = function () {
                /*******Limpio la informacion del servicio ********/
                $scope.qSms = "";
                $scope.fSms = "";
                $scope.aSms = "";
                $scope.qMail = "";
                $scope.fMail = "";
                $scope.aMail = "";
                $scope.qPush = "";
                $scope.fPush = "";
                $scope.aPush = "";
                $scope.qDevice = "";
                $scope.fDevice = "";
                $scope.aDevice = "";
                $scope.qWa = "";
                $scope.fWa = "";
                $scope.aWa = "";
                $scope.qSt = "";
                $scope.fSt = "";
                $scope.aSt = "";
                $scope.montoAct = "";
                $scope.montoInst = "";
                $scope.montoInsp = "";
                $scope.montoUsers = "";

            }


            //**********change tope************/
            $scope.changeTope = function () {

                if ($("#frecuencies").val() !== "") {
                    $http({
                        url: "getcomboboxServices",
                        method: "post",
                        data: {tope: $scope.agreement.tope_n_customers, frecuency: $("#frecuencies").val()}
                    }).then(function successCallback(response) {
                        $scope.agreement.frecuency = $("#frecuencies").val();
                        $scope.clearInfo();
                        /*********Seteando data en los array para llenar los combobox**********/
                        if (response.data.activations.length > 0) {
                            $scope.activations = response.data.activations;
                        } else {
                            $scope.activations = [];
                        }

                        if (response.data.installations.length > 0) {
                            $scope.installations = response.data.installations;
                        } else {
                            $scope.installations = [];
                        }

                        if (response.data.inspections.length > 0) {
                            $scope.inspections = response.data.inspections;
                        } else {
                            $scope.inspections = [];
                        }

                        if (response.data.users.length > 0) {
                            $scope.users = response.data.users;
                        } else {
                            $scope.users = [];
                        }

                        if (response.data.sms.length > 0) {
                            $scope.sms = response.data.sms;
                            $scope.selectedSms = response.data.sms[0].id;
                            $scope.qSms = $scope.sms[0].quantity;
                            $scope.fSms = $scope.sms[0].description;
                            $scope.aSms = $scope.sms[0].amount;

                            if ($scope.sms[0].mandatory == 1) {
                                $("#smscheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aSms));
                                $scope.agreement.sms = $scope.selectedSms;
                            } else {
                                $("#smscheck").attr("disabled", false).attr("checked", false);
                            }
                        } else {
                            $scope.sms = []
                        }

                        if (response.data.mails.length > 0) {
                            $scope.mails = response.data.mails;
                            $scope.selectedMail = response.data.mails[0].id;
                            $scope.qMail = $scope.mails[0].quantity;
                            $scope.fMail = $scope.mails[0].description;
                            $scope.aMail = $scope.mails[0].amount;
                            if ($scope.mails[0].mandatory == 1) {
                                $("#mailcheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aMail));
                                $scope.agreement.mail = $scope.selectedMail;
                            } else {
                                $("#mailcheck").attr("disabled", false).attr("checked", false);
                            }
                        } else {
                            $scope.mails = [];
                        }

                        if (response.data.pushs.length > 0) {
                            $scope.pushs = response.data.pushs;
                            $scope.selectedPush = response.data.pushs[0].id;
                            $scope.qPush = $scope.pushs[0].quantity;
                            $scope.fPush = $scope.pushs[0].description;
                            $scope.aPush = $scope.pushs[0].amount;
                            if ($scope.pushs[0].mandatory == 1) {
                                $("#pushcheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aPush));
                                $scope.agreement.push = $scope.selectedPush;
                            } else {
                                $("#pushcheck").attr("disabled", false).attr("checked", false);
                            }
                        } else {
                            $scope.pushs = [];
                        }

                        if (response.data.devices.length > 0) {
                            $scope.devices = response.data.devices;
                            $scope.selectedDivice = response.data.devices[0].id;
                            $scope.qDevice = $scope.devices[0].quantity;
                            $scope.fDevice = $scope.devices[0].description;
                            $scope.aDevice = $scope.devices[0].amount;
                            if ($scope.devices[0].mandatory == 1) {
                                $("#devcheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aDevice));
                                $scope.agreement.device = $scope.selectedDivice;
                            } else {
                                $("#devcheck").attr("disabled", false).attr("checked", false);
                            }
                        } else {
                            $scope.devices = [];
                        }

                        if (response.data.web_access.length > 0) {
                            $scope.web_access = response.data.web_access;
                            $scope.selectedWA = response.data.web_access[0].id;
                            $scope.qWa = $scope.web_access[0].quantity;
                            $scope.fWa = $scope.web_access[0].description;
                            $scope.aWa = $scope.web_access[0].amount;
                            if ($scope.web_access[0].mandatory == 1) {
                                $("#awebcheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aWa));
                                $scope.agreement.wa = $scope.selectedWA;
                            } else {
                                $("#awebcheck").attr("disabled", false).attr("checked", false)
                            }
                            ;
                        } else {
                            $scope.web_access = [];
                        }

                        if (response.data.suport.length > 0) {
                            $scope.suport = response.data.suport;
                            $scope.selectedST = response.data.suport[0].id;
                            $scope.qSt = $scope.suport[0].quantity;
                            $scope.fSt = $scope.suport[0].description;
                            $scope.aSt = $scope.suport[0].amount;
                            if ($scope.suport[0].mandatory == 1) {
                                $("#stcheck").attr("disabled", true).attr("checked", true);
                                $scope.base = (parseFloat($scope.base) + parseFloat($scope.aSt));
                                $scope.agreement.st = $scope.selectedST;
                            } else {
                                $("#stcheck").attr("disabled", false).attr("checked", false);
                            }
                        } else {
                            $scope.suport = [];
                        }
                    });
                } else {
                    $scope.agreement.tope_n_customers = "";
                    applicationService.generateNoty("danger", "Usted debe seleccionar una frecuencia de facturación");
                }
            }

            $scope.selectedPackage = function (model, type, input) {
                var service = $("#" + type).val();
                if (service > 0) {
                    $.each(model, function (index, value) {
                        if (value.id == service) {
                            if ($("#" + input).is(":checked")) {
                                $scope.base = (parseFloat($scope.base) + parseFloat(value.amount));
                            } else {
                                $scope.base = (parseFloat($scope.base) - parseFloat(value.amount));
                            }
                        }
                    });
                }
            }

            $scope.chengeMenus = function (type) {

                switch (type) {
                    case 'activations':
                        for (var x = 0; x < $scope.activations.length; x++) {
                            if ($("#" + type).val() > 0) {
                                if ($scope.activations[x].id == $scope.agreement.activations) {
                                    $scope.montoAct = $scope.activations[x].amount;
                                    $scope.base = (parseFloat($scope.base) + parseFloat($scope.montoAct));
                                }
                            } else {

                                $scope.base = (parseFloat($scope.base) - parseFloat($scope.montoAct));
                            }
                        }
                        break;
                    case 'inspections':
                        for (var x = 0; x < $scope.inspections.length; x++) {
                            if ($("#" + type).val() > 0) {
                                if ($scope.inspections[x].id == $scope.agreement.inspections) {
                                    $scope.montoInsp = $scope.inspections[x].amount;
                                    $scope.base = (parseFloat($scope.base) + parseFloat($scope.montoInsp));
                                } else {
                                    $scope.base = (parseFloat($scope.base) - parseFloat($scope.montoInsp));
                                }
                            }
                        }
                        break;
                    case 'installations':
                        for (var x = 0; x < $scope.installations.length; x++) {
                            if ($("#" + type).val() > 0) {
                                if ($scope.installations[x].id == $scope.agreement.installations) {
                                    $scope.montoInst = $scope.installations[x].amount;
                                    $scope.base = (parseFloat($scope.base) + parseFloat($scope.montoInst));
                                }
                            } else {
                                $scope.base = (parseFloat($scope.base) - parseFloat($scope.montoInst));
                            }
                        }
                        break;
                    case 'users':
                        for (var x = 0; x < $scope.users.length; x++) {
                            if ($("#" + type).val() > 0) {
                                if ($scope.users[x].id == $scope.agreement.users) {
                                    $scope.montoUsers = $scope.users[x].amount;
                                    $scope.base = (parseFloat($scope.base) + parseFloat($scope.montoUsers));
                                }
                            } else {
                                $scope.base = (parseFloat($scope.base) - parseFloat($scope.montoInst));
                            }
                        }
                        break;
                }

            }


            /**********Cnange de servicios*************/
            $scope.changeServices = function (type) {

                if ($("#" + type).val() > 0) {
                    switch (type) {
                        case 'sms':
                            for (var x = 0; x < $scope.sms.length; x++) {
                                if ($scope.sms[x].id == $scope.agreement.sms) {
                                    $scope.qSms = $scope.sms[x].quantity;
                                    $scope.fSms = $scope.sms[x].description;
                                    $scope.aSms = $scope.sms[x].amount;
                                    ($scope.sms[x].mandatory == 1) ? $("#smscheck").attr("disabled", true).attr("checked", true) : $("#smscheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                        case 'mail':
                            for (var x = 0; x < $scope.mails.length; x++) {
                                if ($scope.mails[x].id == $scope.agreement.mail) {
                                    $scope.qMail = $scope.mails[x].quantity;
                                    $scope.fMail = $scope.mails[x].description;
                                    $scope.aMail = $scope.mails[x].amount;
                                    ($scope.mails[x].mandatory == 1) ? $("#mailcheck").attr("disabled", true).attr("checked", true) : $("#mailcheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                        case 'push':
                            for (var x = 0; x < $scope.pushs.length; x++) {
                                if ($scope.pushs[x].id == $scope.agreement.push) {
                                    $scope.qPush = $scope.pushs[x].quantity;
                                    $scope.fPush = $scope.pushs[x].description;
                                    $scope.aPush = $scope.pushs[x].amount;
                                    ($scope.pushs[x].mandatory == 1) ? $("#pushcheck").attr("disabled", true).attr("checked", true) : $("#pushcheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                        case 'device':
                            for (var x = 0; x < $scope.devices.length; x++) {
                                if ($scope.devices[x].id == $scope.agreement.device) {
                                    $scope.qDevice = $scope.devices[x].quantity;
                                    $scope.fDevice = $scope.devices[x].description;
                                    $scope.aDevice = $scope.devices[x].amount;
                                    ($scope.devices[x].mandatory == 1) ? $("#devcheck").attr("disabled", true).attr("checked", true) : $("#devcheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                        case 'wa':
                            for (var x = 0; x < $scope.web_access.length; x++) {
                                if ($scope.web_access[x].id == $scope.agreement.wa) {
                                    $scope.qWa = $scope.web_access[x].quantity;
                                    $scope.fWa = $scope.web_access[x].description;
                                    $scope.aWa = $scope.web_access[x].amount;
                                    ($scope.web_access[x].mandatory == 1) ? $("#awebcheck").attr("disabled", true).attr("checked", true) : $("#awebcheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                        case 'st':
                            for (var x = 0; x < $scope.suport.length; x++) {
                                if ($scope.suport[x].id == $scope.agreement.st) {
                                    $scope.qWa = $scope.suport[x].quantity;
                                    $scope.fWa = $scope.suport[x].description;
                                    $scope.aWa = $scope.suport[x].amount;
                                    ($scope.suport[x].mandatory == 1) ? $("#stcheck").attr("disabled", true).attr("checked", true) : $("#stcheck").attr("disabled", false).attr("checked", false);
                                }
                            }

                            break;
                    }
                }
            }

            $scope.submit = function () {

                if ($("#formAgreements .ng-valid")) {

                    if ($scope.agreement.activations > 0 || $scope.agreement.inspections > 0 || $scope.agreement.installations) {

                        if ($scope.agreement.users > 0) {
                            $scope.agreement.menus = [$scope.activations, $scope.inspections, $scope.installations, $scope.users];
                            $scope.agreement.servicesPackage = [$scope.sms, $scope.mails, $scope.pushs, $scope.devices, $scope.web_access, $scope.suport];
                            $scope.agreement.m_status = ($("#mstatus").is(":checked")) ? 1 : 0;
                            $scope.agreement.sms = ($("#smscheck")).is(":checked") ? $scope.selectedSms : "";
                            $scope.agreement.mail = ($("#mailcheck")).is(":checked") ? $scope.selectedMail : "";
                            $scope.agreement.push = ($("#pushcheck")).is(":checked") ? $scope.selectedPush : "";
                            $scope.agreement.device = ($("#devcheck")).is(":checked") ? $scope.selectedDivice : "";
                            $scope.agreement.wa = ($("#awebcheck")).is(":checked") ? $scope.selectedWA : "";
                            $scope.agreement.st = ($("#stcheck")).is(":checked") ? $scope.selectedST : "";

                            AgreementsCompanies.save($scope.agreement,
                                success = function (response) {
                                    if (response.msg) {
                                        angular.copy({}, $scope.agreement);
                                        applicationService.generateNoty("success", " Nuevo Contrato creado!!");
                                    }
                                }, error = function (response) {
                                    var msg = applicationService.getMsgErrors(response.data);
                                    applicationService.generateNoty("danger", msg);
                                });
                        } else {
                            applicationService.generateNoty("danger", "Usuarios master es requerido!");
                        }
                    } else {
                        applicationService.generateNoty("danger", "Usted debe seleccionar una opción de menú!");
                    }

                }
            }
    }])

    .controller("editAgreementsCompaniesCtrl", ["$scope", "AgreementsCompanies", "$routeParams", "applicationService", "$http", function ($scope, AgreementsCompanies, $routeParams, applicationService, $http) {

        $scope.settings = {module: "Contratos de empresas", action: "agreementsCompanies", title: "Editar" ,icon: "mdi-editor-mode-edit", botonclass: "btn-primary"};
        $scope.items = [{id: "1", name: "SysAdmin"}, {id: "2", name: "Bussines"}, {id: "3", name: "Customer"}];

        $scope.submit = function () {
            AgreementsCompanies.update($scope.agreements,
                success = function (response) {
                    if (response.msg) {
                        applicationService.generateNoty("success", " AgreementsCompanies actualizado !");
                    }
                }, error = function (response) {
                    var msg = applicationService.getMsgErrors(response.data);
                    applicationService.generateNoty("danger", msg);
                });
        }
    }])

