angular.module('newApp')
/****Routing****/
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/agreements/customers/create', {
            templateUrl: 'md-layout/angularjs/app/agreements/customers/form.html',
            controller: 'createAgreementsCtrl'
        });
        $routeProvider.when('/agreements/maps', {
            templateUrl: 'md-layout/angularjs/app/agreements/customers/maps.html',
            controller: 'mapsCtrl'
        });
    }])
/****Controllers****/
    .controller('agreementsCtrl', ["$scope", "ngTableParams", "AgreementsCustomers", "$http", "$resource", "$route", "$routeParams", "applicationService",
        function ($scope, ngTableParams, AgreementsCustomers, $http, $resource, $route, $routeParams, applicationService) {

            $scope.settings = {module: "Contratos del Cliente", action: "agreements/customers" ,title: "Listado", icon: "mdi-content-add"};
            $scope.agreementsSelected = [];

            var buscar = "";
            var status = "";

            $scope.filter = function () {
                buscar = ($scope.buscar !== "") ? $scope.buscar : "";
                status = ($scope.status !== "" && $scope.status !== undefined) ? $scope.status : "";
                $scope.tableParams.reload();
            }

            $scope.tableParams = new ngTableParams({
                    page: 1,          // primera página a mostrar
                    count: 15,
                    sorting: {id: "desc"},
                    filter: {country: ""}
                },
                {
                    getData: function ($defer, params) {
                        var url = 'agreementscustomers?search=' + buscar + '&status=' + status + '&page=' + params.page() + '&per_page=' + params.count() + '&order=' + params.orderBy();
                        $http({url: url, method: "get"}).then(function successCallback(response) {
                            $scope.agreementsSelected = [];
                            $scope.start = response.data.from;
                            $scope.end = response.data.to;
                            $scope.total = response.data.total;
                            $scope.tableParams.start(response.data.from);
                            $scope.tableParams.end(response.data.to);//Hasta donde esta mostrado
                            $scope.tableParams.total(response.data.total);//Total de registros
                            return $defer.resolve(response.data.data)
                        });
                    },
                });

            $scope.annullar = function (id) {
                if (confirm("¿Está seguro de  borrar este elemento?")) {
                    AgreementsCustomers.delete({id: id}).$promise.then(function (data) {
                        if (data.code == 200) {
                            applicationService.generateNoty("success", data.msg);
                            $scope.tableParams.reload();
                        } else {
                            applicationService.generateNoty("danger", data.msg);
                        }
                    })
                }
            }

            $scope.viewAgreement = function(id)
            {
                AgreementsCustomers.get({id: id}, function (response) {
                    $scope.planes = response.data.services;
                    $scope.customer = response.data.customer;
                });

                $("#btnmodal").trigger("click");
            }

            $scope.selecteRow = function (row, id, $index) {
                if($("#"+row).attr("class") == "ng-scope active"){
                    $("#"+row).removeClass("active");
                    $scope.agreementsSelected.splice($index, 1);
                }else{
                    $("#"+row).addClass("active");
                    $scope.agreementsSelected.push({id:id});
                }
            }

        }])

    .controller('createAgreementsCtrl', ["$scope", "AgreementsCustomers", "applicationService", "$http", function ($scope, AgreementsCustomers, applicationService, $http) {

        $scope.settings = {module: "Contratos del Cliente", action: "agreements/customers", title:'Nuevo', icon: "mdi-content-add", botonclass: "btn-success"};

        $scope.monto = 0;
        $scope.tax = 12;
        $scope.total_amount = 0;
        $scope.planes = [];


        $scope.agreement = {
            customer: "",
            planes: "",
            companies: "",
            address: "",
            longitude: "",
            latitude: "",
            country: "",
            states: "",
            town: "",
            parish: ""
        };

        $scope.securityCompanies = [];

        $http({url: "services/getservices", method: "post"}).then(function successCallback(response) {
            $scope.services = response.data;
        });

        $scope.arrayAsyncCompanies = {
            query: function (query) {

                if (query.term == "") {
                    query.callback({results: [{id: 0, text: "Ingrese una palabra a buscar"}]});
                } else {
                    $http({
                        url: "companies/searchsecuritycompanies",
                        method: "post",
                        data: {search: query.term, companies: $scope.securityCompanies}
                    }).then(function successCallback(response) {
                        query.callback({results: response.data});
                    });
                }
            },
        };


        $scope.arrayAsync = {
            query: function (query) {

                if (query.term == "") {
                    query.callback({results: [{id: 0, text: "Ingrese una palabra a buscar"}]});
                } else {
                    $http({
                        url: "customers/autocomplete",
                        method: "post",
                        data: {search: query.term}
                    }).then(function successCallback(response) {
                        query.callback({results: response.data});
                    });
                }
            },
        };

        $scope.addPlan = function (id, name, inning, number, amount, index) {
            $scope.monto = (parseFloat($scope.monto) + parseFloat(amount));
            $scope.planes.push({id: id, name: name, inning: inning, number_visit: number, amount: amount});
            $scope.services.splice(index, 1);
        }

        $scope.addCompany = function (objeto) {
            console.log(objeto);
            if ($scope.agreement.companies !== "") {
                $scope.securityCompanies.push({id: objeto.id, text: objeto.text});
                $scope.agreement.companies = "";
            } else {
                applicationService.generateNoty("warning", "Primero debe seleccionar una empresa");
            }
        }

        $scope.removeCompany = function (index) {
            $scope.securityCompanies.splice(index, 1);
        }

        $scope.removePlan = function (index, plan) {
            $scope.planes.splice(index, 1);
            $scope.monto = parseFloat($scope.monto) - parseFloat(plan.amount);

            $scope.services.push({
                id: plan.id,
                name: plan.name,
                inning: plan.inning,
                number_visit: plan.number_visit,
                amount: plan.amount
            });
        }

        $scope.map = {center: {latitude: 18.0709778, longitude: -29.0485218 }, zoom: 3 };
        $scope.options = {scrollwheel: false};
        $scope.marker = {
            id: 0,
            coords: {
                latitude:  18.0709778,
                longitude: -29.0485218
            },
            options: { draggable: true },
            events: {
                dragend: function (marker, eventName, args) {
                    $scope.map.center.latitude =  marker.getPosition().lat();
                    $scope.map.center.logitude =  marker.getPosition().lng();
                    $scope.company.latitude = marker.getPosition().lat();
                    $scope.company.longitude = marker.getPosition().lng();
                }
            }
        };

        $scope.geocoding = function (address) {
            $http({
                url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + address,
                method: "get"
            }).then(function successCallback(response) {
                if (response.data.status === "OK") {
                    var location = response.data.results[0].geometry.location;
                    $scope.map.zoom = 13;
                    $scope.marker.coords.latitude = location.lat;
                    $scope.marker.coords.longitude = location.lng;
                    $scope.map.center.latitude = location.lat;
                    $scope.map.center.longitude = location.lng;
                    $scope.agreement.latitude = location.lat;
                    $scope.agreement.longitude = location.lng;
                }
            });
        }

        $http({url: "countries/getcombobox", method: "post"}).then(function successCallback(response) {
            $scope.countries = response.data;
        });

        $scope.getStates = function () {
            //*******Reset to states, town and parish**********/
            $scope.states = [];
            $scope.towns = [];
            $scope.parish = [];

            $http({
                url: "states/getcombobox",
                method: "post",
                data: {"country": $scope.agreement.country}
            }).then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.states = response.data;
                    $scope.disabledState = "";

                } else {
                    $scope.states = [];
                }
            });
        }

        $scope.getTowns = function () {
            $http({
                url: "town/getcombobox",
                method: "post",
                data: {"states": $scope.agreement.states}
            }).then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.towns = response.data;
                    $scope.disabledTown = "";
                } else {
                    $scope.towns = [];
                }
            });
        }

        $scope.getParishs = function () {
            $http({
                url: "parish/getcombobox",
                method: "post",
                data: {"town": $scope.agreement.town}
            }).then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.parishs = response.data;
                    $scope.disabledParish = "";
                } else {
                    $scope.parishs = [];
                }
            });
        }

        $scope.submit = function () {

            if ($("#formCustomerAgreement .ng-valid")) {
                $scope.agreement.planes = $scope.planes;
                $scope.agreement.companies = $scope.securityCompanies;

                AgreementsCustomers.save($scope.agreement,
                    success = function (response) {
                        if (response.msg) {
                            angular.copy({}, $scope.agreement);

                            $scope.securityCompanies = [];
                            $scope.planes = [];
                            $scope.monto = 0;

                            $scope.marker.coords.latitude = 18.0709778;
                            $scope.marker.coords.longitude = -29.0485218;

                            applicationService.generateNoty("success", " Nueva contrato registrada!");
                        }
                    }, error = function (response) {
                        var msg = applicationService.getMsgErrors(response.data);
                        applicationService.generateNoty("danger", msg);
                    });
            }else {
                applicationService.generateNoty("danger", "El formulario aún no se ha validado!");
            }

        }
    }])
    .controller('mapsCtrl', ["$scope", "applicationService", "$http", function ($scope, applicationService, $http) {
        $scope.$on('$viewContentLoaded', function () {

            $scope.map = {center: {latitude: 40.1451, longitude: -99.6680 }, zoom: 4 };
            $scope.options = {scrollwheel: true};
            var events = {
                places_changed: function (searchBox) {
                    console.log($scope);
                }
            }
        });
    }]);



