angular.module('newApp')
/****Routing****/
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/roles/create', {
            templateUrl: 'md-layout/angularjs/app/roles/form.html',
            controller: 'CreateRolesCtrl'
        });
        $routeProvider.when('/roles/edit/:id', {
            templateUrl: 'md-layout/angularjs/app/roles/form.html',
            controller: 'EditRolesCtrl'
        });
    }])
/****Controllers****/
    .controller('rolesCtrl', ["$scope", "ngTableParams", "Roles", "$http", "$resource", "$route", "$routeParams", "applicationService",
        function ($scope, ngTableParams, Roles, $http, $resource, $route, $routeParams, applicationService) {

            $scope.settings = {module: "Roles", action: "roles" ,title: "Listado", icon: "mdi-content-add"};
            $scope.checkboxModel = false;
            $scope.rolesSelected = [];

            var buscar = "";
            $scope.filter = function () {
                buscar = $scope.buscar;
                $scope.tableParams.reload();
            }

            $scope.tableParams = new ngTableParams({
                    page: 1,          // primera página a mostrar
                    count: 20,
                    sorting: {id: "desc"}
                },
                {
                    getData: function ($defer, params) {
                        var url = 'roles?search=' + buscar + '&page=' + params.page() + '&per_page=' + params.count() + '&order=' + params.orderBy();
                        $http({url: url, method: "get"}).then(function successCallback(response) {
                            $scope.rolesSelected = [];
                            $scope.start = response.data.from;
                            $scope.end = response.data.to;
                            $scope.total = response.data.total;
                            $scope.tableParams.start(response.data.from);
                            $scope.tableParams.end(response.data.to);//Hasta donde esta mostrado
                            $scope.tableParams.total(response.data.total);//Total de registros
                            return $defer.resolve(response.data.data)
                        });
                    },
                });

            $scope.edit = function (id){
                $scope.rolesSelected = [];
                window.location.href = "#/roles/edit/"+id;
            }

            $scope.remove = function (id) { //Step 1
                $("#modal-slideleft").modal();
                $("#yes").click(function(){
                    applicationService.removeItems("roles", $scope.rolesSelected);
                    setTimeout(function () {
                        $scope.tableParams.reload();
                    }, 1000);
                });
            }

            $scope.selecteRow = function (row, id, $index) {
                if($("#"+row).attr("class") == "ng-scope active"){
                    $("#"+row).removeClass("active");
                    $scope.rolesSelected.splice($index, 1);
                }else{
                    $("#"+row).addClass("active");
                    $scope.rolesSelected.push({id:id});
                }
            }
        }])

    .controller('CreateRolesCtrl', ["$scope", "Roles", "applicationService", "$http", function ($scope, Roles, applicationService, $http) {

        $scope.settings = {module: "Roles", action: "roles", title:'Nuevo', icon: "mdi-content-add", botonclass: "btn-success"};

        $http({
            url: "checkprivilegio",
            method: 'GET'})
            .then(function successCallback(response) {
                if(response.data.admin){
                    $scope.items = [{id: "1", name: "SysAdmin"}, {id: "2", name: "Bussines"}, {id: "3", name: "Customer"}];
                }else{
                    $scope.items = [{id: "2", name: "Bussines"}, {id: "3", name: "Customer"}];
                }
            });

        $scope.role = {
            name: "",
            slug: "",
            description: "",
            level: ""
        };

        $scope.refresh = function()
        {
            $scope.role = angular.copy({});
            return false;
        }

        $scope.submit = function () {
            if ($("#formRole .ng-valid")) {
                Roles.save($scope.role,
                    success = function (response) {
                        if (response.msg) {
                            angular.copy({}, $scope.role);
                            applicationService.generateNoty("success", " Rol Guardado!");
                        }
                    }, error = function (response) {
                        var msg = applicationService.getMsgErrors(response.data);
                        applicationService.generateNoty("danger", msg);
                    });
            } else {
                applicationService.generateNoty("danger", "Corrija datos en el formulario!");
            }
        }
    }])
    
    .controller("EditRolesCtrl", ["$scope", "Roles", "$routeParams", "applicationService", function ($scope, Roles, $routeParams, applicationService) {

        $scope.settings = {module: "Roles", action: "roles", title: "Editar" ,icon: "mdi-editor-mode-edit", botonclass: "btn-primary"};
        $scope.items = [{id: "1", name: "SysAdmin"}, {id: "2", name: "Bussines"}, {id: "3", name: "Customer"}];

        var id = $routeParams.id;
        Roles.get({id: id}, function (data) {
            $scope.role = data.role;
        });

        $scope.refresh = function()
        {
            Roles.get({id: id}, function (data) {
                $scope.role = data.role;
            });
        }

        $scope.submit = function () {
            Roles.update($scope.role,
                success = function (response) {
                    if (response.msg) {
                        applicationService.generateNoty("success", " Rol editado!");
                        setTimeout(function () {
                            window.location = "#/roles";
                        }, 1500);
                    }
                }, error = function (response) {
                    var msg = applicationService.getMsgErrors(response.data);
                    applicationService.generateNoty("danger", msg);
                });
        }
    }])

