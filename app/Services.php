<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Services extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['inning','name','number_visit','days','hour_from', 'hour_to', 'amount', 'mstatus'];

    public function scopeSearch($query,$search)
    {
        $query->select("services.id", "status.description as inning", "services.name", "services.number_visit", "services.days", "services.hour_from", "services.hour_to", "services.amount", "services.mstatus");
        $query->join('status', 'status.id', '=', 'services.inning');

        if($search[0] !== ""){
            $query->where("services.name","LIKE","%".$search[0]."%");
            $query->orWhere("services.number_visit","LIKE","%".$search[0]."%");
            $query->orWhere("services.amount","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("services.mstatus",$search[1]);
        }
    }
}