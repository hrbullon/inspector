<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspections extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inspections';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer', 'type', 'user', 'vehicle', 'agreement', 'latitude', 'longitude', 'sent'];


    /***********RELATIONS**************/
    /**
     * Get the Customer for the Company .
     */
    public function UserCompany()
    {
        return $this->belongsTo('App\Customers', 'customer');
    }


    public function scopeSearch($query, $search)
    {
        $query->select('customers.first_name', 'inspections.id','customers.first_surname', 'inspections.vehicle','inspections.date_attended','users.firstname','users.lastname','status.description as type');
        $query->join('customers', 'customers.id', '=', 'inspections.customer');
        $query->join('users', 'users.id', '=', 'inspections.user');
        $query->join('status', 'status.id', '=', 'inspections.type');

        if($search[0] != "")
        {
            $fechas = explode(",",$search[0]);
            $fecha1 = $fechas[0];
            $fecha2 = $fechas[1];

            if($fecha1<$fecha2){
                $query->whereBetween("actived_alarms.activation_date", array($fecha1,$fecha2));
            }      
        }
    }

    public static function InsertInspection($activation, $customer)
    {
        $inspection = new Inspections();
        $inspection->customer = $customer;
        $inspection->type = 79;
        $inspection->activation = $activation;
        $inspection->user = 2;
        $inspection->date_created = date('Y-m-d H:i:s');
        $inspection->save();

        return $inspection->id;

    }
}
