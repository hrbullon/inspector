<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivedAlarms extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actived_alarms';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['activation_date', 'account'];

    /***********RELATIONS**************/
    /**
     * Get the Commnets for the activations .
     */
    public function ActivationComments()
    {

        



        //return $this->hasMany('App\CommentsActivation','activation');
    }

    /**
     * Get the zoning for the activations .
     */
    public function ActivationZoning()
    {
        return $this->hasMany('App\ActivedAlarmsZonning','activation');
    }

    public function scopeSearch($query,$search)
    {
        $query->select('actived_alarms.id','alarm_count.account','customers.first_name','customers.first_surname',
            'actived_alarms.activation_date','users.firstname','users.lastname','actived_alarms.mstatus');
        $query->join('alarm_count', 'alarm_count.id', '=', 'actived_alarms.account');
        $query->join('customers', 'customers.id', '=', 'alarm_count.customer');
        $query->join('users', 'users.id', '=', 'actived_alarms.created_by');

        if($search[0] != null)
        {
            $query->where("alarm_count.account","LIKE","%".$search[0]."%");
            $query->orWhere("users.firstname","LIKE","%".$search[0]."%");
            $query->orWhere("users.lastname","LIKE","%".$search[0]."%");
            $query->orWhere("customers.first_name","LIKE","%".$search[0]."%");
            $query->orWhere("customers.first_surname","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("actived_alarms.mstatus",$search[1]);
        }
    }

    public static function getActivations($id){

        return $rs = \DB::table('actived_alarms')
                ->join('alarm_count', 'alarm_count.id', '=', 'actived_alarms.account')
                ->select('actived_alarms.id','alarm_count.account','alarm_count.name', 'actived_alarms.activation_date', 'actived_alarms.mstatus')
                ->where('alarm_count.customer', $id)
                ->get();
    }
}
