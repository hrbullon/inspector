<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'banks';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['country', 'name', 'short_name','prefix','web_page', 'master_phone', 'conformation_phone'];

    public function scopeSearch($query, $search)
    {
        $query->select("banks.id","countries.name as country","banks.name","banks.short_name","banks.prefix", "banks.web_page", "banks.master_phone", "banks.conformation_phone");
        $query->join('countries', 'countries.id', '=', 'banks.country');

        if($search[0] != ""){
            $query->where("banks.name","LIKE","%".$search[0]."%");
            $query->orWhere("banks.short_name","LIKE","%".$search[0]."%");
            $query->orWhere("banks.prefix","LIKE","%".$search[0]."%");
        }
        
        if($search[1] != null)
        {
            $query->where("banks.country",$search[1]);
        }

    }
}