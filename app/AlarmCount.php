<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlarmCount extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'alarm_count';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /***********RELATIONS**************/
    /**
     * Get the Company for the installer .
     */
    public function CompanyAlarm()
    {
        return $this->belongsTo('App\Companies','company');
    }

    /**
     * Get the Customer for the installer .
     */
    public function CustomerAlarm()
    {
        return $this->belongsTo('App\Customer','customer');
    }

    /**
     * Get the Zoning for the installer .
     */
    public function ZoningAlarm()
    {
        return $this->hasMany('App\ZoningAlarm','alarm_count');
    }

    protected $fillable = ['customer', 'protocol', 'account','name','country','states','town','parish',
        'latitude','longitude','address','mstatus'];

    public function scopeSearch($query, $search)
    {
        $query->select('customers.first_name','customers.first_surname','alarm_count.id','alarm_count.name','alarm_count.account','alarm_count.mstatus','status.description as protocol');
        $query->join('customers', 'customers.id', '=', 'alarm_count.customer');
        $query->join('status', 'status.id', '=', 'alarm_count.protocol');

        if($search[0] != null)
        {
            $query->where("alarm_count.account", "LIKE", "%".$search[0]."%");
            $query->orWhere("alarm_count.name", "LIKE", "%".$search[0]."%");
            $query->orWhere("status.description", "LIKE", "%".$search[0]."%");
            $query->orWhere("customers.first_name", "LIKE", "%".$search[0]."%");
            $query->orWhere("customers.first_surname", "LIKE", "%".$search[0]."%");
        }
    }
}
