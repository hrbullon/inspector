<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountStatus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account_status';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['company', 'customer', 'type','agreement_company','agreement_customer',
    'renewals','payout','date','description','debit','credit','balance_register','balance_account'];

    public static function getAccountStatus($type, $id)
    {
         if($type == 1)
         {
             $clausula = " agreements.customer = " . $id;
             $key = "crypt_id_agreements_customers";
         }else{
             $clausula = "agreements_companies.company = " . $id;
             $key = "crypt_id_agreements_companies";
         }

        $sql = "select account_status.id,date_format(account_status.date,'%d-%m-%Y') as date, account_status.type,  
                case account_status.type
                    when  88 then  account_status.agreement_company
                    when  89 then  account_status.agreement_customer
                    when  90 then  account_status.renewals
                    when  91 then  account_status.renewals
                    when  96 then  account_status.payout
                    when  97 then  account_status.payout
                    when  98 then  account_status.payout
                else  'Undefined'
                end as number, 
                account_status.description, account_status.debit, account_status.credit, account_status.balance_register, account_status.balance_account
                from  account_status
                left join  agreements on agreements.id = account_status.agreement_customer
                left join  agreements_companies on agreements_companies.id = account_status.agreement_company
                left join  renewals on renewals.id = account_status.renewals
                left join  payout on payout.id = account_status.payout
                where ". $clausula." order by date ";
        $rs =  \DB::select($sql);

        $data = [];

        foreach ($rs as $value)
        {
            $array = [
                "id" => $value->id,
                "date" => $value->date,
                "type" => $value->type,
                "number" => $value->number,
                "key" => \Crypt::encrypt($value->number.$key),
                "description" => $value->description,
                "debit" => $value->debit,
                "credit" => $value->credit,
                "balance_register" => $value->balance_register,
                "balance_account" => $value->balance_account,
            ];
            array_push($data,$array);
        }
        return $data;
    }
}
