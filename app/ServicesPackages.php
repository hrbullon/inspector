<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesPackages extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services_packages';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['country', 'type', 'quantity', 'amount', 'frecuency', 'tope_n_customers', 'm_status', 'mandatory'];

    public function scopeSearch($query,$search)
    {
        $query->select("services_packages.id","countries.name as country","status.description as type",
                       "services_packages.quantity","services_packages.amount","services_packages.frecuency",
                       "services_packages.tope_n_customers", "services_packages.m_status", "services_packages.mandatory");
        $query->join('countries', 'countries.id', '=', 'services_packages.country');
        $query->join('status', 'status.id', '=', 'services_packages.type');

        if($search[0] != null)
        {
            $query->where("services_packages.country",$search[0]);
        }
        if($search[1] != null)
        {
            $query->where("services_packages.type",$search[1]);
        }
        if($search[2] != null)
        {
            $query->where("services_packages.frecuency",$search[2]);
        }
        if($search[3] != null)
        {
            $query->where("services_packages.m_status",$search[3]);
        }
    }
}
