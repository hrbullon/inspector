<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsServices extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_services';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public static function insertAgreementServices($agreement,$services,$amount)
    {
            $service = new AgreementsServices();
            $service->agreement = $agreement;
            $service->services = $services;
            $service->amount = $amount;
            $service->tax_id = 1;//$services[$x]["tax_id"];
            $service->tax_amount = Taxes::getTaxTotal(1,$amount);
            $service->total_amount = ($service->tax_amount + $service->amount);
            $service->save();
    }

    public static function getServices($agreement)
    {
        return \DB::table('agreements_services')
            ->join('services', 'services.id', '=', 'agreements_services.services')
            ->select('services.*')->where('agreements_services.agreement',$agreement)
            ->get();
    }

    public static function getServicesView($agreement)
    {
        $query = "select
                    services.name
                    , services.inning
                    , services.number_visit
                    , agreements_services.amount
                from
                    agreements_services
                    inner join services 
                        on (agreements_services.services = services.id)
                where  agreements_services.agreement = ". $agreement;
        return \DB::select($query);
    }
}
