<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'latitude', 'longitude', 'zoom', 'gtm', 'area_code', 'format_dni', 'icon'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("name","LIKE","%".$search[0]."%");
        }
    }
}
