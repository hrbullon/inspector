<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsCompaniesDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_companies_detail';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['agreements_comp', 'service_package'];

    /**
     * Get agreements
     */
    public function agreements()
    {
        return $this->belongsTo('App\agreements_companies', 'foreign_key');
    }

    public function scopeSearch($query, $search)
    {

    }
    
    public static function getServices($agreement)
    {
        return \DB::table('agreements_companies_detail')
            ->join('services_packages', 'services_packages.id', '=', 'agreements_companies_detail.service_package')
            ->select('services_packages.*')->where('agreements_companies_detail.agreements_comp',$agreement)
            ->get();
    }

    public static function getServicesPackage($agreement)
    {
        $query  =  " select sp.type, t.description as package, sp.quantity, sp.tope_n_customers
                     from agreements_companies_detail ad
                        inner join services_packages sp on sp.id = ad.service_package
                        inner join status t on (t.id = sp.type and t.source = 13)
                     where ad.agreements_comp = ".$agreement." order by t.id ";
        return \DB::select($query);
    }
}
