<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Taxes extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'taxes';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country', 'name', 'aliquot', 'applicable_from', 'm_status', 'predetermined'];

    public function scopeSearch($query,$search)
    {
        $query->select("taxes.id","countries.name as country","taxes.name","taxes.aliquot","taxes.applicable_from","taxes.m_status");
        $query->join('countries', 'countries.id', '=', 'taxes.country');
        if($search[0] != null)
        {
            $query->where("taxes.name","LIKE","%".$search[0]."%");
            $query->orWhere("countries.name","LIKE","%".$search[0]."%");
            $query->orWhere("taxes.aliquot","LIKE","%".$search[0]."%");
            $query->orWhere("taxes.applicable_from","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("taxes.country",$search[1]);
        }
        if($search[2] != null)
        {
            $query->where("taxes.m_status",$search[2]);
        }
    }

    public static function getTaxTotal($tax, $monto)
    {
        $country = Session::get("country");
        $tax = Taxes::where("country",$country)->where("predetermined",1)->where("m_status",1)->get();
        return (($monto * $tax->aliquot) + $monto);
    }

    public static function getTax($amount)
    {
        $country = Session::get("country");
        $tax = \DB::table("taxes")->where("country",$country)->where("predetermined",1)->where("m_status",1)->get();
        return [$tax[0]->id, ($amount * $tax[0]->aliquot)/100];
    }
}
