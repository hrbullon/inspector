<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoningAlarm extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'zoning_alarm';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['alarm_count', 'zone', 'name', 'zoom'];

}
