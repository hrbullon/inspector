<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionView extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_view';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['permission', 'role_view'];

    public function scopeSearch($query, $search)
    {
        if($search != null)
        {

        }

    }
}
