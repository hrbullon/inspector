<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'status';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['source', 'value', 'description'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("description","LIKE","%".$search[0]."%");
            $query->orWhere("source",$search[1]);
            $query->orWhere("value",$search[2]);
        }

    }
}
