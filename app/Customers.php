<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function countries()
    {
        return $this->belongsTo('App\Countries','country');
    }

    public function states()
    {
        return $this->belongsTo('App\States','state');
    }

    public function towns()
    {
        return $this->belongsTo('App\Town','town');
    }

    protected $fillable = ['type','c_identity','first_name','second_name','first_surname', 'second_surname', 'address',
        'country', 'state', 'town', 'parish', 'contact_person', 'mstatus', 'ispotential'];

    public function scopeSearch($query,$search)
    {
        if($search[0] !== ""){
            $query->where("customers.first_name","LIKE","%".$search[0]."%");
            $query->orWhere("customers.second_name","LIKE","%".$search[0]."%");
            $query->orWhere("customers.first_surname","LIKE","%".$search[0]."%");
            $query->orWhere("customers.second_surname","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("customers.type",$search[1]);
        }
        if($search[2] != null)
        {
            $query->where("customers.mstatus",$search[2]);
        }
    }
}
