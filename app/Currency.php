<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'currency';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['country', 'name', 'plural', 'abbreviated', 'exchange_purchase', 'exchange_sale'];

    public function scopeSearch($query,$search)
    {
        $query->select("currency.id","countries.name as country","currency.name","currency.plural","currency.abbreviated","currency.exchange_purchase","currency.exchange_sale");
        $query->join('countries', 'countries.id', '=', 'currency.country');
        if($search[0] != null)
        {
            $query->where("currency.name","LIKE","%".$search[0]."%");
            $query->orWhere("countries.name","LIKE","%".$search[0]."%");
            $query->orWhere("currency.plural","LIKE","%".$search[0]."%");
            $query->orWhere("currency.abbreviated","LIKE","%".$search[0]."%");
            $query->orWhere("currency.exchange_purchase","LIKE","%".$search[0]."%");
            $query->orWhere("currency.exchange_sale","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("currency.country",$search[1]);
        }
    }
}
