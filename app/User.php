<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("c_identity","LIKE","%".$search[0]."%");
            $query->orWhere("username","LIKE","%".$search[0]."%");
            $query->orWhere("firstname","LIKE","%".$search[0]."%");
            $query->orWhere("lastname","LIKE","%".$search[0]."%");
            $query->orWhere("email","LIKE","%".$search[0]."%");
        }
        if(!self::isAdmin())
        {
            $query->orWhere("type_identity",">",1);
        }

    }

    /**
     * Check ROLE is number one for user logueado  .
     */
    public static function isAdmin()
    {
        //Role 1 es SuperAdmin
        $admin = RoleUser::where("user",Auth::user()->id)->where("role",1)->get();
        //Tiene registros ?
        return (count($admin) > 0)? true : false;
    }

    public static function canMake($action)
    {
        $byRole = "SELECT count(1) as flag FROM role_user
                INNER JOIN roles ON roles.id = role_user.role
                INNER JOIN roles_view ON roles_view.role = roles.id
                INNER JOIN view ON view.id = roles_view.view
                INNER JOIN permission_view ON permission_view.role_view = roles_view.id
                INNER JOIN permissions ON permissions.id = permission_view.permission
                WHERE role_user.user = ? AND CONCAT(view.action,'/',permissions.slug) = ?";


        $byUser = "SELECT * FROM permission_user
                INNER JOIN permissions ON permissions.id = permission_user.permission_id
                WHERE permission_user.user = ?  AND permissions.slug = ?";

        $rs1 = \DB::select($byRole, [Auth::user()->id,'roles/create']);
        $rs2 = \DB::select($byUser, [Auth::user()->id,'roles/create']);

        return  ($rs1[0]->flag > 0 || $rs2[0]->flag > 0)? true : false;
    }

    public static function getPermissionsByView($view)
    {
        $sql = "SELECT * FROM role_user
                INNER JOIN roles ON roles.id = role_user.role
                INNER JOIN roles_view ON roles_view.role = roles.id
                INNER JOIN VIEW ON view.id = roles_view.view
                INNER JOIN permission_view ON permission_view.role_view = roles_view.id
                INNER JOIN permissions ON permissions.id = permission_view.permission
                WHERE role_user.user = 5 AND view.action = ?";

        return \DB::select($sql, [Auth::user()->id,$view]);

    }
}
