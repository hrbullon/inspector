<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleView extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles_view';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['role', 'view'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {

        }

    }

    public static function verifyPermitsViews()
    {
        RoleView::where();
    }
}
