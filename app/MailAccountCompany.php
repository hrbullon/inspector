<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class MailAccountCompany extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mail_accounts_company';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'domain', 'mail_address', 'password', 'incoming_server', 'ssl_incoming_requested',
    'incoming_port', 'incoming_user', 'outgoing_server', 'outgoing_port', 'outgoing_user', 'ssl_outgoing_requested'];

    public static function setParams()
    {
        $mail = \DB::table('mail_accounts_company')->first();
        if(sizeof($mail) > 0 )
        {
            Config::set('mail.driver',$mail->driver);
            Config::set('mail.host',$mail->outgoing_server);
            Config::set('mail.port',$mail->outgoing_port);
            Config::set('mail.username',$mail->outgoing_user);
            Config::set('mail.password',$mail->password);
            Config::set('mail.encryptation', ($mail->ssl_outgoing_requested == 1)? "ssl" : "tls");

            return true;
        }
        return false;
    }
}
