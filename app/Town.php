<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'town';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['states', 'name', 'area_code'];

    public function scopeSearch($query, $search)
    {
        $query->select("town.id","states.name as states","town.name","town.area_code");
        $query->join('states', 'states.id', '=', 'town.states');
        if($search[0] != null)
        {
            $query->where("town.name","LIKE","%".$search[0]."%");
            $query->where("town.states","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("town.states","LIKE","%".$search[1]."%");
        }
    }
}
