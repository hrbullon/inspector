<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreements extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer', 'date', 'latitude', 'longitude', 'address', 'seller', 'mstatus', 'country', 'states', 'town', 'parish'];


    /********** Relationship**********/
    public function services()
    {
        return $this->hasMany('App\AgreementsServices','agreement');
    }

    public function customers()
    {
        return $this->belongsTo('App\Customers', 'customer');
    }

    public function countries()
    {
        return $this->belongsTo('App\Countries','country');
    }

    public function states()
    {
        return $this->belongsTo('App\States','state');
    }

    public function towns()
    {
        return $this->belongsTo('App\Town','town');
    }

    public function scopeSearch($query, $search)
    {
        $query->select("agreements.id","customers.first_name","customers.first_surname","users.firstname","users.lastname",
            "agreements.date","agreements.amount","agreements.mstatus");
        $query->join('users', 'users.id', '=', 'agreements.seller');
        $query->join('customers', 'customers.id', '=', 'agreements.customer');

        if($search[0] != null)
        {
            $query->where("customers.first_name","LIKE","%".$search[0]."%");
            $query->orWhere("customers.first_surname","LIKE","%".$search[0]."%");
            $query->orWhere("customers.c_identity","LIKE","%".$search[0]."%");
        }
    }

    public static function getAgreementsCustomer($customer)
    {
        $query = "select 
        agreements.id as agreement, date_format(agreements.date, '%d-%m-%Y'),agreements.mstatus as status,
        sum(agreements_services.total_amount) as amount
        from  agreements 
        inner join customers on customers.id = agreements.customer
        inner join agreements_services on agreements_services.agreement = agreements.id
        where customer = ".$customer." group by agreements.id";

        return \DB::select($query);
    }

    public function addDetails($request, $agreetment)
    {
        $accumulator = 0;
        for ($x = 0; $x < count($request["planes"]); $x++)
        {
            AgreementsServices::insertAgreementServices($agreetment->id, $request["planes"][$x]['id'],$request["planes"][$x]['amount']);
            $accumulator = ($accumulator+$request["planes"][$x]['amount']);
        }

        for ($x = 0; $x < count($request["companies"]); $x++)
        {
            AgreementsSecurityCompany::insertSecurityCompanies($agreetment->id, $request["companies"][$x]['id']);
        }

        //asigno el monto al contrato
        $agreetment->amount = $accumulator;

        //Agrego a la renovacion
        $renewals = new Renewals();
        $idRenewals = $renewals->insertRenewals($agreetment,86);
        //Obtengo los servicios contratados;
        $services = AgreementsServices::getServices($agreetment->id);
        $renewalsDetails = new RenewalsDetails();
        $renewalsDetails->insertRenewalsDetails($idRenewals,$services,86);

        $accountStatus = new AccountStatus();
        $accountStatus->type = 89;
        $accountStatus->agreement_customer = $agreetment->id;
        $accountStatus->date = $agreetment->date;
        //Obtengo el  nombre de la empresa;
        $customer = Customers::find($agreetment->customer);
        $accountStatus->description = 'Emision de Contrato Cliente ' . $customer->first_name .'  '.  $customer->first_surname .' Nro. ' .$agreetment->id;
        $accountStatus->debit = $agreetment->amount;
        $accountStatus->credit = 0;
        $accountStatus->balance_register = $agreetment->amount;
        $accountStatus->balance_account = $agreetment->amount;
        $accountStatus->save();

        return $accumulator;
    }
}
