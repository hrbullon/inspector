<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role', 'user'];

    /***********RELATIONS**************/
    /**
     * Get the USER for the permission .
     */
    public function RoleUser()
    {
        return $this->belongsTo('App\Users','user');
    }
    /***********END RELATIONS**************/



    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {

        }

    }
}
