<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankingTerminal extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banking_terminal';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['company', 'bank_account', 'number','description','m_status'];

    /***********RELATIONS**************/
    /**
     * Get the Bank Accounts for the banking terminal .
     */
    public function BankAcount()
    {
        return $this->belongsTo('App\BankAccounts','bank_account');
    }

    /**
     * Get the Company for the bank termina .
     */
    public function Companies()
    {
        return $this->belongsTo('App\Companies','company');
    }

    public function scopeSearch($query, $search)
    {
        $query->select("banking_terminal.id","banks.name","bank_accounts.account","banking_terminal.number",
            "banking_terminal.description","banking_terminal.m_status");
        $query->join('bank_accounts', 'bank_accounts.id', '=', 'banking_terminal.bank_account');
        $query->join('banks', 'banks.id', '=', 'bank_accounts.bank');

        if($search[0] != null)
        {
            $query->where("bank_accounts.account","LIKE","%".$search[0]."%");
            $query->orWhere("banking_terminal.number","LIKE","%".$search[0]."%");
            $query->orWhere("banking_terminal.description","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("banks.id",$search[1]);
        }
        if($search[2] != null)
        {
            $query->where("banking_terminal.m_status",$search[2]);
        }


    }
}

