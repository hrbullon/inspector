<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionsUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_user';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['permission_id', 'user'];


    /***********RELATIONS**************/
    /**
     * Get the USER for the permission .
     */
    public function PermissionUser()
    {
        return $this->belongsTo('App\Users','user');
    }


    /**
     * Get permissions for the user .
     */
    public function getPermission()
    {
        return $this->belongsTo('App\Permissions','permission_id');
    }
    /***********END RELATIONS**************/


    public function scopeSearch($query, $search)
    {
        if($search != null)
        {
            $query->where("permission_id","LIKE","%".$search."%");
        }

    }
}
