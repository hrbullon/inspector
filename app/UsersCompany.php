<?php

namespace App;

USE Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UsersCompany extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_company';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'company'];


    /***********RELATIONS**************/
    /**
     * Get the USER for the Company .
     */
    public function UserCompany()
    {
        return $this->belongsTo('App\Users', 'user');
    }

    /**
     * Get the Companies for the user.
     */
    public function Company()
    {
        return $this->belongsTo('App\Companies','company');
    }

    /***********END RELATIONS**************/

    public function scopeSearch($query, $search)
    {
        if ($search[0] != null) {

        }

    }

    public static function dropDownCompanies()
    {
        $userCompany = \DB::table('user_company')
            ->join('companies', 'companies.id', '=', 'user_company.company')
            ->select(['user_company.company','companies.logotype', 'companies.name'])->where("user_company.user", Auth::user()->id)->get();

        $dropDown = '<li class="dropdown" id="language-header">';
            if(count($userCompany) == 1)
            {
                $dropDown .= '<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-globe"></i>
                                <span>'.  $userCompany[0]->name .'</span>
                              </a>';
            }
            if(count($userCompany) > 1)
            {
                $dropDown = '<li class="dropdown" id="language-header">';

                for ($x = 0; $x < count($userCompany); $x++) {

                   if ($userCompany[$x]->company == Session::get('company')) {
                        $dropDown .= '<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                          <i class="icon-globe"></i>
                                          <span>' . $userCompany[$x]->name . '</span>
                                        </a>';
                    }
                }
				
                $dropDown .=  '<ul class="dropdown-menu">';
                for ($i = 0; $i < count($userCompany); $i++) {
					
					if ($userCompany[$i]->company !== Session::get('company')) {
						$dropDown .= '<li>
						   <a href="changecompany/'.\Crypt::encrypt($userCompany[$i]->company)  .' " data-lang="en"> <span>'.$userCompany[$i]->name.'</span></a>
						 </li>';
					}	
				}
                $dropDown .=  '</ul>';
            }
        $dropDown .= '</li>';
        echo $dropDown;
    }
}
