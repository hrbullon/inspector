<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bank_accounts';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['bank', 'belong', 'company','account','type','m_status'];

    /***********RELATIONS**************/
    /**
     * Get the Bank for the banks .
     */
    public function BankAcount()
    {
        return $this->belongsTo('App\Banks','bank');
    }

    /**
     * Get the Company for the bank account .
     */
    public function AccountsCompanies()
    {
        return $this->belongsTo('App\Companies','company');
    }

    public function scopeSearch($query, $search){
        
        $query->select("bank_accounts.id", "banks.name as bank", "bank_accounts.belong", "companies.name as company", "bank_accounts.account", "status.description as type", "bank_accounts.m_status");
        $query->join('status', 'status.id', '=', 'bank_accounts.type');
        $query->join('banks', 'banks.id', '=', 'bank_accounts.bank');
        $query->join('companies', 'companies.id', '=', 'bank_accounts.company');

        if($search[0] != null)
        {
            $query->where("bank_accounts.account", "LIKE", "%".$search[0]."%");
            $query->orWhere("banks.name", "LIKE", "%".$search[0]."%");
            $query->orWhere("companies.name", "LIKE", "%".$search[0]."%");
            $query->orWhere("status.description", "LIKE", "%".$search[0]."%");
            $query->orWhere("bank_accounts.m_status", "LIKE", "%".$search[0]."%");
        }
        /*if($search[1] != null)
        {
            $query->where("bank_accounts.bank",$search[1]);
        }*/
    }
}