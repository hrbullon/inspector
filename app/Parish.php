<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parish';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['town', 'name'];

    public function scopeSearch($query, $search)
    {
        $query->select("parish.id","town.name as town","parish.name");
        $query->join('town', 'town.id', '=', 'parish.town');
        if($search[0] != "")
        {
            $query->where("parish.name","LIKE","%".$search[0]."%");
        }
    }
}
