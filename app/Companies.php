<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function UsersCompany()
    {
        return $this->hasMany('App\UsersCompany', 'company');
    }

    public function countries()
    {
        return $this->belongsTo('App\Countries','country');
    }

    public function states()
    {
        return $this->belongsTo('App\States','state');
    }

    public function towns()
    {
        return $this->belongsTo('App\Town','town');
    }

    protected $fillable = [
        'c_identity', 'name', 'alias', 'logotype',
        'address', 'type', 'handles_credit', 'credit_days',
        'credit_limit', 'type_taxpayer', 'creation_date', 'country',
        'state','town','parish','city','zip',
        'latitude','longitude','m_status',
    ];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("name","LIKE","%".$search[0]."%");
            $query->orWhere("c_identity","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("m_status",$search[1]);
        }
    }
}
