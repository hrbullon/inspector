<?php

namespace App;

use Crypt;
use Illuminate\Database\Eloquent\Model;

class PaymentMethods extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_methods';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'date_mandatory', 'reference_mandatory','bank_mandatory','ps_mandatory','lot_mandatory','conformation_mandatory','type_tax_mandatory'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("payment_methods.name","LIKE","%".$search[0]."%");
        }
    }
}
