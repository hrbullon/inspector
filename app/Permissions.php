<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'model'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("name","LIKE","%".$search[0]."%");
            $query->orWhere("slug","LIKE","%".$search[0]."%");
            $query->orWhere("description","LIKE","%".$search[0]."%");
        }
        if($search[1])
        {
            $query->where("id",">",4);
        }

    }

    public static function getItems($pa = 0, $per_page = null, $page = null)
    {
        $parents  =  Views::all()->where("parent",$pa)->forPage($per_page,$page);
        $data = [];

        if (sizeof($parents) > 0) {
            foreach($parents as $parent)
            {
                $array["number"] = $parent->id;
                $array["name"] = $parent->name;
                $array["name_check"] = str_replace(" ", "_", $parent->name);
                $array["icon"] = $parent->icon;
                $array["parent"] = $parent->parent;
                $array["child"] = self::getItems($parent->id);
                array_push($data, $array);
            }
        }
        return $data;
    }

}
