<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsCompanies extends Model
{
    public $service_package;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_companies';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company', 'frecuency', 'tope_n_customers', 'seller',
        'date', 'type_sale', 'amount', 'total_amount', 'm_status'
    ];

    /**
     * Get the details for the agreements .
     */
    public function details()
    {
        return $this->hasMany('App\AgreementsCompaniesDetail','agreements_comp');
    }

    /**
     * Get the details for the Companies .
     */

    public function companies()
    {
        return $this->belongsTo('App\Companies','company');
    }

    public function frecuencies()
    {
        return $this->belongsTo('App\Status','frecuency');
    }

    public function sales()
    {
        return $this->belongsTo('App\Status','type_sale');
    }

    public function sellers()
    {
        return $this->belongsTo('App\User','seller');
    }

    public function scopeSearch($query, $search)
    {
        $query->select("agreements_companies.id","companies.name as company","users.firstname","users.lastname",
            "agreements_companies.date","agreements_companies.amount","agreements_companies.m_status");
        $query->join('users', 'users.id', '=', 'agreements_companies.seller');
        $query->join('companies', 'companies.id', '=', 'agreements_companies.company');

        if($search[0] != null)
        {
            $query->where("companies.name","LIKE","%".$search[0]."%");
            $query->orWhere("companies.c_identity","LIKE","%".$search[0]."%");
        }
    }

    public function addDetails($request, $agreetment)
    {
        $accumulator = 0;
        //Agrego los menus contratados
        foreach($request['menus'] as $value){
            if($request['activations'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['inspections'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['installations'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['users'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
        }

        //Agrego los servicios contratados
        foreach($request['servicesPackage'] as $value){
            if($request['sms'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['mail'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['push'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['device'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['wa'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
            if($request['st'] == $value[0]['id'])
            {
                $this->insertDetail($agreetment, $value[0]['id'], $value[0]['amount']);
                $accumulator += $value[0]['amount'];
            }
        }

        $model =  AgreementsCompanies::find($agreetment);
        $model->amount = $accumulator;
        //Si guardo los detalles del contrato
        if($model->save())
        {
            $renewals = new Renewals();
            $idRenewals = $renewals->insertRenewals($model,85);
            //Obtengo los servicios contratados;
            $services = AgreementsCompaniesDetail::getServices($agreetment);
            $renewalsDetails = new RenewalsDetails();
            $renewalsDetails->insertRenewalsDetails($idRenewals,$services,85);

            $accountStatus = new AccountStatus();
            $accountStatus->type = 88;
            $accountStatus->agreement_company = $model->id;
            $accountStatus->date = $model->date;
            //Obtengo el  nombre de la empresa;
            $company = Companies::find($model->company);
            $accountStatus->description = 'Emision de Contrato Empresa ' . $company->name .' Nro. ' .$model->id;
            $accountStatus->debit = $model->amount;
            $accountStatus->credit = 0;
            $accountStatus->balance_register = $model->amount;
            $accountStatus->balance_account = $model->amount;
            $accountStatus->save();
        }
    }

    public function insertDetail($agreetment,$package,$amount)
    {
        $agreemetnDetails = new AgreementsCompaniesDetail();
        $agreemetnDetails->agreements_comp = $agreetment;
        $agreemetnDetails->service_package = $package;

        $tax = Taxes::getTax($amount);
        $agreemetnDetails->amount = $amount;
        $agreemetnDetails->tax_id = $tax[0];
        $agreemetnDetails->tax_amount = $tax[1];
        $agreemetnDetails->total_amount = ($agreemetnDetails->tax_amount+$amount);

        $agreemetnDetails->save();
    }

    public static function getAgreementsCompany($company)
    {
        $query = "select 
        agreements_companies.id as agreement, date_format(agreements_companies.date, '%d-%m-%Y') as date,
        agreements_companies.amount, agreements_companies.m_status as status
        from  agreements_companies 
        inner join companies on companies.id = agreements_companies.company
        where agreements_companies.company = ". $company;

        return \DB::select($query);
    }
}
