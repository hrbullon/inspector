<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class CommentsActivation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments_activation';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['activation', 'operator', 'account','operator'];

    public static function InsertComments($activation, $request)
    {
        for($x=0; $x < count($request['comments']); $x++)
        {
            $activedComment = new CommentsActivation();
            $activedComment->activation = $activation->id;
            $activedComment->operator = Session::get('user');
            $activedComment->comment = $request['comments'][$x]['comment'];
            $activedComment->save();
        }
    }

    public static function GetComments($activation)
    {
        return DB::table('comments_activation')
            ->join('users', 'users.id', '=', 'comments_activation.operator')
            ->select('comments_activation.comment','comments_activation.date','users.id', 'users.firstname', 'users.lastname')
            ->where('comments_activation.activation',$activation)->get();
    }

}
