<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSchedules extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_schedules';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'innings', 'from', 'end'];

    public static function getEvents(){

        $sql = "select concat(u.firstname,\"  \",u.lastname) as title, us.from as start, us.end, 
        if(us.innings = 1, \"bg-yelow\", if(us.innings = 2, \"bg-blue\", \"bg-dark\")) as className 
        from users_schedules us
        inner join users u on u.id =  us.user
        where us.from between concat(date_format( now(), \"%Y-%m-\"),\"01\") and last_day(now())";

        return \DB::select($sql);
    }
}
