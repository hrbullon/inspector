<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Views extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'view';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['name', 'email', 'password'];
    protected $fillable = ['name', 'action', 'icon' , 'parent', 'mstatus'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("name","LIKE","%".$search[0]."%");
            $query->orWhere("action","LIKE","%".$search[0]."%");
        }
        if($search[1] !=null)
        {
            $query->where("mstatus",$search[1]);
        }
    }

    private static function getItems($parent = null)
    {
        //Filtro que permite traer los parent items y children
        $clausula = ($parent !== null)?  $parent  : 0 ;

        if($parent == null)
        {
            $rs =  \DB::table("view")->where("parent",0)->get();
        }else{
            //Is admin
            if(User::isAdmin()){
                $rs =  \DB::table("view")->where("parent",$clausula)->get();
            }else{

                $rs =  \DB::table("permission_view")
                    ->select("view.id","view.name", "view.action", "view.icon")
                    ->distinct()
                    ->join("roles_view", "permission_view.role_view", "=", "roles_view.id")
                    ->join("role_user", "role_user.role", "=", "roles_view.role")
                    ->join("permissions", "permission_view.permission", "=", "permissions.id")
                    ->join("view", "roles_view.view", "=", "view.id")
                    ->where("view.parent", $clausula)->where("role_user.user",Session::get("user"))
                    ->where("permission_view.permission", 4)->get();
            }
        }
        return $rs;
    }

    public static function getPanelItems()
    {
        $flag = 0;
        $item = "";
        $parents = Views::getItems();
        foreach($parents as $parent)
        {
            $childrens = Views::getItems($parent->id);
            if(sizeof($childrens) > 0)
            {
                if($flag == 0 )
                {
                    $item .= " <li class='nav-parent'>
                                <a href='$parent->action'>
                                    <i class='$parent->icon'></i>
                                    <span> $parent->name </span>
                                    <span class='fa arrow'></span>
                                </a>";
                }
                $item .= "<ul class=\"children collapse\">";
                foreach($childrens as $children)
                {
                    $item .= "  <li ng-class=\"{ active  : isActive('/$children->action')}\" role='$children->action'><a href='#$children->action'> $children->name</a></li>";
                }
                $item .= "</ul>";
                $item .= "</li>";
            }elseif($parent->action !== "" ){
                $item .= "<li><a href='#$parent->action'><i class=".$parent->icon."></i><span>". $parent->name ."</span></a></li>";
            }
        }
        echo $item;
    }


}
