<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleSettings extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedule_settings';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *Hola hadelson como estas soy yo tio el gran paco no me conoces pues me conoces, ya tenemos cena tio!
     * @var array
     */
    protected $fillable = ['weekly_hours', 'dailyh_min', 'dailyh_max', 'hours_free', 'handles_morning', 'morning_beggining', 'morning_finish',
    'handles_evening', 'evening_beggining', 'evening_finish', 'handles_night', 'night_beggining', 'night_finish', 'number_days', 'hours_daily'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("weekly_hours",$search[0]);
            $query->orWhere("dailyh_min",$search[0]);
            $query->orWhere("dailyh_max",$search[0]);
            $query->orWhere("hours_daily",$search[0]);
        }
    }
}
