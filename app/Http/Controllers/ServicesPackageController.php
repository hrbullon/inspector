<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\ServicesPackages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ServicesPackageForm;
use App\Http\Controllers\Controller;

class ServicesPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","services_packages.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","services_packages.".$request->get("order"));
            $sort  = "asc";
        }
        $params = [$request->get("country"), $request->get("type"),$request->get("frecuency"),$request->get("status")];
        $rs = ServicesPackages::search($params)->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_services_packages"),
                "country"=>$values->country,
                "type"=>$values->type,
                "quantity"=>$values->quantity,
                "amount"=>$values->amount,
                "frecuency"=>$values->frecuency,
                "tope_n_customers"=>$values->tope_n_customers,
                "m_status"=>$values->m_status,
                "mandatory"=> $values->mandatory
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicesPackageForm $request)
    {
        /*****Validacion personalizada (Un paquete de servicio no puede tener la misma frecuencia e
         ****** igual configuración con distinto precio.)******************************************/
        $rs = DB::table('services_packages')->where("country",$request["country"])
            ->where("type",$request["type"])->where("quantity",$request["quantity"])
            ->where("frecuency",$request["frecuency"])->where("tope_n_customers",$request["tope_n_customers"])
            ->where("mandatory",$request["mandatory"])->where("amount","<>",$request["amount"])->get();

        if(sizeof($rs) > 0)
        {
            return response()->json([
                "msg" => ["Ya existe un paquete de servicio con estas configuraciones, pero con un monto diferente"],
            ], 422);
        }else{
            $services = new ServicesPackages($request->all());
            $services->quantity = (isset($request["quantity"]) && $request["quantity"] !== "") ? $request["quantity"] : "0";
            $services->tope_n_customers = (isset($request["tope_n_customers"]) && $request["tope_n_customers"] !== "") ? $request["tope_n_customers"] : "0";
            $services->save();
            return response()->json([
                "msg" => "success",
                "id" => $services->id
            ], 200);
        }
  }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $servicesPackage = ServicesPackages::find($decrypt);
        if (sizeof($servicesPackage) > 0) {
            $servicesPackage->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "servicesPackage" => $servicesPackage,
        ], $code);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesPackageForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $servicepackage = ServicesPackages::find($decrypt);
        $servicepackage->fill($request->all());
        $servicepackage->save();

        return response()->json([
            "msg" => "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if ($servicepackage = ServicesPackages::find($decrypt)) {
            $servicepackage->m_status = 0;
            $servicepackage->save();
            $msg = "Paquete de Servicio eliminado satisfactoriamente!";
            $code = 200;
        }else{
            $msg = "Error, paquete de servicio no encontrado!";
            $code = 404;
        }
        return response()->json([
            "msg"    =>  $msg,
            "code"   =>  $code
        ], $code);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            $servicepackage = ServicesPackages::find($decrypt);
            $servicepackage->m_status = 0;
            $servicepackage->save();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request)
    {
        $services = [
            "sms" => DB::table('services_packages')
                        ->join("status", "services_packages.frecuency", "=", "status.id")
                            ->where('type', 41)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "mails" => DB::table('services_packages')
                        ->join("status", "services_packages.frecuency", "=", "status.id")
                            ->where('type', 42)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "devices" => DB::table('services_packages')
                            ->join("status", "services_packages.frecuency", "=", "status.id")
                                ->where('type', 43)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                    ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "web_access" => DB::table('services_packages')
                                ->join("status", "services_packages.frecuency", "=", "status.id")
                                    ->where('type', 44)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                        ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "suport" => DB::table('services_packages')
                            ->join("status", "services_packages.frecuency", "=", "status.id")
                                 ->where('type', 45)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                   ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "pushs"=>DB::table('services_packages')
                        ->join("status", "services_packages.frecuency", "=", "status.id")
                            ->where('type', 46)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount","services_packages.mandatory"]),

            "activations"=>DB::table('services_packages')
                                ->join("status", "services_packages.frecuency", "=", "status.id")
                                    ->where('type', 54)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                        ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount"]),

            "inspections"=>DB::table('services_packages')
                                ->join("status", "services_packages.frecuency", "=", "status.id")
                                    ->where('type', 55)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                        ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount"]),

            "installations"=>DB::table('services_packages')
                                ->join("status", "services_packages.frecuency", "=", "status.id")
                                    ->where('type', 56)->where("tope_n_customers", $request["tope"])->where("frecuency",$request["frecuency"])
                                         ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount"]),

            "users"=>DB::table('services_packages')
                        ->join("status", "services_packages.frecuency", "=", "status.id")
                            ->where('type', 57)->where("frecuency",$request["frecuency"])
                                ->get(["services_packages.id","services_packages.tope_n_customers","status.description","services_packages.quantity","services_packages.amount"]),
        ];
        return response()->json($services);
    }

}
