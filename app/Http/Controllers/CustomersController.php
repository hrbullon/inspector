<?php

namespace App\Http\Controllers;

use App\ActivedAlarms;
use App\Inspections;
use App\Sessions;
use App\User;
use Crypt;

use App\Customers;
use App\CustomersContact;
use App\Agreements;
use App\AlarmCount;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CustomersForm;
use App\Http\Requests\CustomersContactForm;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Session;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","customers.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","customers.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Customers::search([$request->get("search"),$request->get("type"), $request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_customers"),
                "type"=>$values->type,
                "c_identity"=>$values->c_identity,
                "first_name"=>$values->first_name,
                "first_surname"=>$values->first_surname,
                "address"=>$values->address,
                "mstatus"=>$values->mstatus,
                "name_row"=>'customers_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomersForm $request)
    {
        //id de la empresa debe ser tomado de una variable de session
        $customer = new Customers($request->all());
        $customer->company = Session::get('company');

        //Si guardo  el cliente, entonces tambien guardo los datos de contactos.
        if($customer->save()){
            CustomersContact::insertContact($customer->id, $request['contact']);
        }

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($customer->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $customer = Customers::find($decrypt);
        if (sizeof($customer) > 0) {
            $customer->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "customer" => $customer,
            "contact" => CustomersContact::where('customer',$decrypt)->get()
        ], $code);
    }

    public function getFormatidentity()
    {
        return ["format"=>"/^[G|J|V|]([0-9]{8})/"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function info($id)
    {
        //En este punto puede mirar el  muro un administrador, super user y el usuario propietario.
        $decrypt = Crypt::decrypt($id);
        $customer = Customers::find($decrypt);
        if (sizeof($customer) > 0) {
            $customer->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }

        $contact = CustomersContact::where('customer',$decrypt)->get();
        foreach ($contact as $key => $value) {
           $value->id = Crypt::encrypt($value->id);
        }

        $agreements =  Agreements::where('customer',$decrypt)->get();
        foreach ($agreements as $key => $value) {
           $value->id = Crypt::encrypt($value->id);
        }

        $inspections =  Inspections::where('customer',$decrypt)->get();
        foreach ($inspections as $key => $value) {
           $value->id = Crypt::encrypt($value->id);
        }

        $activations =  ActivedAlarms::getActivations($decrypt);
        foreach ($activations as $key => $value) {
           $value->id = Crypt::encrypt($value->id);
        }

        $alarms =  AlarmCount::where('customer',$decrypt)->get();
        foreach ($alarms as $key => $value) {
           $value->id = Crypt::encrypt($value->id);
        }

        return response()->json([
            "msg" => $msg,
            "customer" => $customer,
            "contact" => $contact,
            "agreements" =>  $agreements,
            "inspections" =>  $inspections,
            "activations" =>  $activations,
            "alarms" =>  $alarms
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomersForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $customer = Customers::find($decrypt);
        $customer->fill($request->all());
        $customer->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    public function addContact(CustomersContactForm $request, $id)
    {
        $contact = new CustomersContact();
        $contact->customer = str_replace("crypt_id_customers","", Crypt::decrypt($id));
        $contact->type = $request["type"];
        $contact->contact = $request["contact"];
        $contact->person = $request["person"];
        $contact->mstatus = $request["mstatus"];
        $contact->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateContact(CustomersContactForm $request)
    {
        $customer_contact = CustomersContact::find($request['id']);
        $customer_contact->fill($request->all());
        $customer_contact->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($customer = Customers::find($decrypt)){
            $customer->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }


    public function autocomplete(Request $request)
    {
        if(User::isAdmin() && $request['referer'] == 'payment/company')
        {
            $customer = \DB::table('companies')->where("name","LIKE","%".$request["search"]."%")->get(["id","name as text"]);

        }
        else{
            $customer = \DB::table('customers')->where("c_identity","LIKE","%".$request["search"]."%")
                ->orWhere("first_name","LIKE","%".$request["search"]."%")->orWhere("first_surname","LIKE","%".$request["search"]."%")
                ->get(["id","first_name as text"]);
        }
        return response()->json($customer);
    }

    public function getCustomer(Request $request){
        return response()->json(DB::table('customers')->get(["id", "first_name"]));
    }

    public function infoContact($id){
        $data = [];
        $rs = CustomersContact::where('id', $id)->get();

        foreach($rs as $values)
        {
            $data = [
                'id'=>$values->id,
                'type'=>$values->type,
                'contact'=>$values->contact,
                'person'=>$values->person,
                'mstatus'=>$values->mstatus,
            ];
        }
        return $data;
    }

    public function destroyContact($id)
    {
        $decrypt = Crypt::decrypt($id);
        $data = [];
        if($customerscontact = CustomersContact::find($decrypt)){
            $customerscontact->mstatus = 0;
            $customerscontact->save();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([ "msg" => $msg], 200);
    }
}
