<?php

namespace App\Http\Controllers;

use App\AccountStatus;
use App\Companies;
use App\Customers;
use Illuminate\Http\Request;

use Crypt;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountStatusController extends Controller
{
    public function customer($id)
    {
        $decrypt = Crypt::decrypt($id);
        $decrypt = str_replace("crypt_id_customers","",$decrypt);

        $customer = Customers::find($decrypt);
        if(sizeof($customer) > 0)
        {
            $customer = [
                "c_identity"=>$customer->c_identity,
                "name" => $customer->first_name. " " . $customer->first_surname,
                "address" => $customer->states->name . " - " . $customer->states->name . " ". $customer->address,
                "m_status" => $customer->mstatus
            ];
        }

        $response = [
            'customer' => $customer,
            'account_status' => AccountStatus::getAccountStatus(1,$decrypt)
        ];
        return response()->json($response);
    }

    public function company($id)
    {
        $decrypt = Crypt::decrypt($id);
        $decrypt = str_replace("crypt_id_companies","",$decrypt);

        $response = [
            'customer' => Companies::find($decrypt),
            'account_status' => AccountStatus::getAccountStatus(2,$decrypt)
        ];
        return response()->json($response);
    }
}
