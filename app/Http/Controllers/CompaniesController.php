<?php

namespace App\Http\Controllers;


use DB;
use Crypt;
use App\Companies;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CompaniesForm;
use App\Http\Controllers\Controller;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","companies.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","companies.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Companies::search([$request["search"],$request["status"]])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_companies"),
                "name"=>$values->name,
                "alias"=>$values->alias,
                "c_identity"=>$values->c_identity,
                "m_status"=>$values->m_status,
                "name_row"=>'companies_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompaniesForm $request)
    {
        $companies = new Companies($request->all());
        $companies->logotype = "logo_example.png";

        if($companies->save()){
            $image = $request["logotype"][0]["result"];
            $path =  $_SERVER['DOCUMENT_ROOT'] . '/inspector/public/assets/companies/images/';
            $type =  explode(';', $image);
            $type =  explode('/', $type[0]);
            list(, $image) = explode(';', $image);
            list(, $image) = explode(',', $image);
            $Base64Img = base64_decode($image);
            file_put_contents($path. $companies->id .'.'. $type[1], $Base64Img);
            $companies->logotype = $companies->id .'.'. $type[1];
            $companies->save();
        }

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($companies->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $company = Companies::find($decrypt);
        if (sizeof($company) > 0) {
            $company->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "company" => $company,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompaniesForm $request, $id)
    {
        //dd($request->all());
        $decrypt = Crypt::decrypt($id);
        $company = Companies::find($decrypt);
        $img = $company->logotype;
        $company->fill($request->all());

        if(is_array($request["logotype"]))
        {
            $path =  $_SERVER['DOCUMENT_ROOT'] . '/inspector/public/assets/companies/images/';
            //unlink($path. $img);
            $image = $request["logotype"]["result"];
            $type =  explode(';', $image);
            $type =  explode('/', $type[0]);
            list(, $image) = explode(';', $image);
            list(, $image) = explode(',', $image);
            $Base64Img = base64_decode($image);
            file_put_contents($path. $company->id .'.'. $type[1], $Base64Img);
            $company->logotype = $company->id .'.'. $type[1];
        }
        $company->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($company = Companies::find($decrypt)){
            $company->delete();
            $response = ["success"=>true, "msg"=>"Se ha eliminado la empresa: <b> $company->name!"];
        }else{
            $response = ["success"=>true, "msg"=>"Error, Empresa no encontrada!"];
        }
        return response()->json($response);
    }


    public function autocomplete(Request $request){
        return response()->json(DB::table('companies')->where("name","LIKE","%".$request["search"]."%")->get(["id","name as text"]));
    }

    public function getFormatidentity()
    {
        return ["format"=>"/^[G|J|V|]([0-9]{8})/"];
    }

    public function getComboBox(Request $request)
    {
        return response()->json(DB::table('companies')->get(["id", "name"]));
    }

    public function searchSecurityCompanies(Request $request)
    {
        if(sizeof($request['companies']) > 0)
        {
            $companies =  DB::table('security_companies')->where("name","LIKE","%".$request["search"]."%")
                ->whereNotIn('id', $request['companies'])->get(["id","name as text"]);
        }else{
            $companies =  DB::table('security_companies')->where("name","LIKE","%".$request["search"]."%")->get(["id","name as text"]);
        }
        return  response()->json($companies);
    }
}
