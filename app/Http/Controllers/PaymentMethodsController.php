<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Crypt;
use App\PaymentMethods;
use App\Http\Requests;
use App\Http\Requests\PaymentMethodsForm;
use App\Http\Controllers\Controller;

class PaymentMethodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","payment_methods.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","payment_methods.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = PaymentMethods::search([$request->get("search")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_payment"),
                "name"=>$values->name,
                "date_mandatory"=>$values->date_mandatory,
                "reference_mandatory"=>$values->reference_mandatory,
                "bank_mandatory"=>$values->bank_mandatory,
                "ps_mandatory"=>$values->ps_mandatory,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentMethodsForm $request)
    {
        $method = PaymentMethods::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($method->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $method = PaymentMethods::find($decrypt);
        if (sizeof($method) > 0) {
            $method->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "payment_methods" => $method,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentMethodsForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $method = PaymentMethods::find($decrypt);
        $method->fill($request->all());
        $method->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($method = PaymentMethods::find($decrypt)){
            $method->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }
}
