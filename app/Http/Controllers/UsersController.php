<?php

namespace App\Http\Controllers;

use App\RoleUser;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;

use DB;
use Crypt;
use App\User;
use App\Http\Requests;
use App\Http\Requests\UserForm;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","users.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","users.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = User::search([$request["search"],$request["status"]])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_users"),
                "username"=>$values->username,
                "firstname"=>$values->firstname,
                "lastname"=>$values->lastname,
                "email"=>$values->email,
                "mstatus"=>$values->mstatus,
                "name_row"=>$values->username.'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User($request->all());
        $user->password = bcrypt($request["username"]);
        $user->avatar = "user.png";

        if ($user->save()) {
            $role = $request["sel_roles"];
            for ($x = 0; $x < count($role); $x++) {
                $roleUser = new RoleUser();
                $roleUser->role = $role[$x];
                $roleUser->user = $user->id;
                $roleUser->save();
            }
            if(sizeof($request["avatar"]) > 0){
                $image = $request["avatar"][0]["result"];
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/inspector/public/assets/global/images/avatars/';
                $type =  explode(';', $image);
                $type =  explode('/', $type[0]);
                list(, $image) = explode(';', $image);
                list(, $image) = explode(',', $image);
                $Base64Img = base64_decode($image);
                file_put_contents($path. $user->id .'.'. $type[1], $Base64Img);
                $user->avatar = $user->id .'.'. $type[1];
                $user->save();
            }
        }

        return response()->json([
            "msg" => "success",
            "id" => $user->id
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $user = User::find($decrypt);
        $itemsRole = [];
        if (count($user->rolesUser) > 0) {
            foreach ($user->rolesUser as $val) {
                array_push($itemsRole, $val["role"]);
            }
        }
        if (sizeof($user) > 0) {
            $user->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "user" => $user,
            "sel_roles" => $itemsRole,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $user = User::find($decrypt);
        $user->fill($request->all());

        if (sizeof($request["sel_roles"]) > 0) {
            RoleUser::query()->where("user", $id)->delete();
            $role = $request["sel_roles"];
            for ($x = 0; $x < count($role); $x++) {
                $roleUser = new RoleUser();
                $roleUser->role = $role[$x];
                $roleUser->user = $user->id;
                $roleUser->save();
            }
        }
        if(is_array($request["avatar"]))
        {
            $path =  $_SERVER['DOCUMENT_ROOT'] . '/inspector/public/assets/global/images/avatars/';
            $image = $request["avatar"]["result"];
            $type =  explode(';', $image);
            $type =  explode('/', $type[0]);
            list(, $image) = explode(';', $image);
            list(, $image) = explode(',', $image);
            $Base64Img = base64_decode($image);
            file_put_contents($path. $user->id .'.'. $type[1], $Base64Img);
            $user->avatar = $user->id .'.'. $type[1];
        }
        $user->save();
        return response()->json([
            "msg" => "success",
        ], 200);
    }

    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($users = User::find($decrypt)){
            $users->mstatus = 0;
            $users->save();
            $msg = "success";
            $code = 200;
        }else{
            $msg = "failed";
            $code = 404;
        }
        return response()->json([
            "msg"    =>  $msg,
            "code"   =>  $code
        ], $code);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]);
            $user = User::find($decrypt);
            $user->mstatus = 0;
            $user->save();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "Usuario Eliminado Satisfactoriamente",
            "flag"   =>  $flag
        ], 200);
    }

    public function autocomplete(Request $request){

        return response()->json(DB::table('users')->where("firstname","LIKE","%".$request["search"]."%")
            ->orWhere("lastname","LIKE","%".$request["search"]."%")->get(["id","firstname as text"]));
    }
}