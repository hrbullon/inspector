<?php

namespace App\Http\Controllers;

use App\AccountStatus;
use App\Companies;
use App\Customers;
use App\Services;
use App\Agreements;
use App\SecurityCompanies;
use App\AgreementsServices;
use App\AgreementsSecurityCompany;

use Crypt;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AgreementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","agreements.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","agreements.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Agreements::search([$request->get("search")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_agreements_customers"),
                "customer"=>$values->c_identity.' '.$values->first_name.' '.$values->first_surname,
                "date"=>date("d-m-Y",strtotime($values->date)),
                "address"=>$values->address,
                "seller"=>$values->firstname.' '.$values->lastname,
                "amount"=>$values->amount,
                "mstatus"=>$values->mstatus,
                "name_row"=>'agreements_customer_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agreements = new Agreements($request->all());
        $agreements->customer = $request{"customer"}["id"];
        $agreements->company = 1; //Session::get('company');
        $agreements->seller = 2; //Session::get('user'); //id del usuario logueado
        $agreements->date = date("Y-m-d H:i:s");
        $agreements->mstatus = 72;

        if($agreements->save())
        {
            $amount = $agreements->addDetails($request,$agreements);
            $agreements->amount = $amount;
            $agreements->save();
        }

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($agreements->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $decrypt = Crypt::decrypt($id);
        $decrypt = str_replace("crypt_id_customers","",$decrypt);
        $agreement = Agreements::find($decrypt);

        if (sizeof($agreement) > 0) {

            $data = [
                'customer'=>[
                    'identity' => $agreement->customers->c_identity,
                    'name' => $agreement->customers->first_name. " ".$agreement->customers->first_surname,
                    'town' => $agreement->towns->name,
                    'address' => $agreement->address,
                    'm_status' => $agreement->customers->mstatus
                ],
                'services'=> AgreementsServices::getServices($decrypt)
            ];

            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }

        return response()->json([
            "msg" => $msg,
            "data" => $data,
        ], $code);
    }

    /**
     * Anull the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($agreement = Agreements::find($decrypt)){
            $agreement->mstatus = 104;
            $agreement->save();
            $msg = "success";
            $code = 200;
        }else{
            $msg = "failed";
            $code = 404;
        }
        return response()->json([
            "msg"    =>  $msg,
            "code"   =>  $code
        ], $code);
    }

    
    public function agreementPdf($id)
    {
        $decrypt = Crypt::decrypt($id);
        $model = Agreements::find($decrypt);
        return \PDF::loadView('reports.agreement_customer', $model)->download('nombre-archivo.pdf');
    }

    public function balance_account($type,$id)
    {
        //Customers
        if($type == 1 )
        {
            $customer = Customers::find($id);
            $account = AccountStatus::where("customer",$id)->get();

        //Companies
        }else{
            $customer = Companies::find($id);
            $account = AccountStatus::where("company",$id)->get();
        }
        return View("reports.balance_account",['type'=>$type,'customer'=>$customer,'account'=>$account]);
    }
}
