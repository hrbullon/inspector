<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Crypt;
use App\BankAccounts;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\BankAccountsForm;
use App\Http\Controllers\Controller;

class BankAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","bank_accounts.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","bank_accounts.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = BankAccounts::search([$request->get("search"), $request->get("bank")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_bank_accounts"),
                "bank"=>$values->bank,
                "account"=>$values->account,
                "type"=>$values->type,
                "m_status"=>$values->m_status,
                "company"=>$values->company,
                "name_row"=>str_replace(" ", "_", $values->bank).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankAccountsForm $request)
    {
        $bankAccount = BankAccounts::create($request->all());

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($bankAccount->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $bankAccount = BankAccounts::find($decrypt);
        if (sizeof($bankAccount) > 0) {
            $bankAccount->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "bankAccount" => $bankAccount,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $bankAccount = BankAccounts::find($decrypt);
        $bankAccount->fill($request->all());
        $bankAccount->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($bankAccount = BankAccounts::find($decrypt)){
            $bankAccount->m_status = 0;
            $bankAccount->save();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            $bankAccount = BankAccounts::find($decrypt);
            $bankAccount->m_status = 0;
            $bankAccount->save();
            $flag = true;
        }

        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getAccount()
    {
        return \DB::table('bank_accounts')
            ->join('banks', 'banks.id', '=', 'bank_accounts.bank')
            ->select('bank_accounts.id','banks.name', 'bank_accounts.account')
            //->where('company',Session::get('company'))
            ->get();
    }
}