<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\Town;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\TownForm;
use App\Http\Controllers\Controller;


class TownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","town.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","town.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Town::search([$request->get("search"),$request->get("state")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_town"),
                "states"=>$values->states,
                "name"=>$values->name,
                "area_code"=>$values->area_code,
                "name_row"=>str_replace(" ", "_", $values->name),
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TownForm $request)
    {
        $town = Town::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  => $town->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $town = Town::find($decrypt);
        if (sizeof($town) > 0) {
            $town->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "town" => $town,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TownForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $town = Town::find($decrypt);
        $town->fill($request->all());
        $town->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($town = Town::find($decrypt)){
            $town->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Town::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request)
    {
        if(isset($request["states"]) && !empty($request["states"]) )
        {
            return response()->json(DB::table('town')->where("states",$request["states"])->get(["id", "name"]));
        }else{
            return response()->json(DB::table('town')->get(["id", "name"]));
        }
    }
}
