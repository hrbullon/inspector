<?php

namespace App\Http\Controllers;


use App\User;
use DB;
use Crypt;
use App\Views;
use App\Roles;
use App\RoleView;
use App\RoleUser;
use App\Permissions;
use App\PermissionView;
use App\PermissionsUser;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PermissionsForm;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","permissions.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","permissions.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Permissions::search([$request["search"],$request["type"]])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_permisssions"),
                "name"=>$values->name,
                "slug"=>$values->slug,
                "description"=>$values->description,
                "name_row"=>str_replace(" ", "_", $values->name).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionsForm $request)
    {
        $permission = new Permissions($request->all());
        $permission->mobile = 0;
        $permission->save();
        return response()->json([
            "msg" => "success",
            "id"  => Crypt::encrypt($permission->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $permission = Permissions::find($decrypt);
        if (sizeof($permission) > 0) {
            $permission->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "permission" => $permission,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionsForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $permission = Permissions::find($decrypt);
        $permission->fill($request->all());
        $permission->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($permission = Permissions::find($decrypt)){
            $permission->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Permissions::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }


    /**
     * Object the specified resource in storage.
     * @param  int $id
     * @return list (menus) and profiles roles
     */
    public function getItems()
    {
        return ["profiles" => Roles::all("id as number", "name", "slug"), "items" => Permissions::getItems()];
    }

    public function getPermissionsUsers(Request $request)
    {
        return $permissions = DB::table('permission_user')->join('permissions', 'permission_user.permission_id', '=', 'permissions.id')
            ->where("user",$request["user"])->get(array(DB::raw('CONCAT(REPLACE(permissions.name, " ", "_"), "_", permissions.id) as text')));
    }


    /**
     * Object the specified resource in storage.
     * @param  int $id
     * @return list permissions
     */
    public function getPermission($id)
    {
        $options = DB::table('permission_view')
            ->join('roles_view', 'permission_view.role_view', '=', 'roles_view.id')
            ->join('permissions', 'permission_view.permission', '=', 'permissions.id')
            ->join('view', 'roles_view.view', '=', 'view.id')
            ->where('roles_view.role', $id)
            ->select('view.name as vista', 'permissions.name as permiso')
            ->get();

        $permission = [];
        foreach ($options as $value) {
            array_push($permission, strtolower($value->permiso) . "-" . str_replace(" ", "_", $value->vista));
        }
        return $permission;
    }

    /*
     * Var $data string "role/parent/children/action/check"
     *
     */
    public function addRevoke($data)
    {
        //Data parse nad convert to array
        if (preg_match("/,/", $data)) {
            $get = substr($data, 0, -1);
            $get = explode(",", $get);
        } else {
            $get = explode(",", $data);
        }
        $flag = false;
        //Move items for insert privilegies
        foreach ($get as $value) {
            $prepare = explode("|", $value);

            //Verifico si el rol tiene permisos a la vista recibida
            $rs = DB::table("roles_view")->where("role", $prepare[0])->where("view", $prepare[2])->first();
            if(count($rs) > 0)
            {
                //Verifico si es assgin o remove
                if($prepare[4] == "checked")
                {
                    //********Verificar que el rol no tenga tal permiso asignado
                    $exist = DB::table('permission_view')->where('permission', $prepare[3])->where('role_view', $rs->id)->first();

                    if(sizeof($exist) !== null){
                        $permission = new PermissionView();
                        $permission->permission = $prepare[3];
                        $permission->role_view = $rs->id;
                        $permission->save();
                        $flag = true;
                    }
                }
                if($prepare[4] == "notchecked")
                {
                    DB::table('permission_view')->where('permission', $prepare[3])->where('role_view', $rs->id)->delete();
                    $flag = true;
                }
            }else{
                /*******En caso de no tenga registros en la tabla de roles views******/
                $rolesView = new RoleView();
                $rolesView->role = $prepare[0];
                $rolesView->view = $prepare[2];
                /*****si logra insertar registros en  la tabla de roles views **********/
                if ($rolesView->save()) {
                    $permission = new PermissionView();
                    $permission->permission = $prepare[3];
                    $permission->role_view = $rolesView->id;
                    $permission->save();
                    $msg = true;
                }
            }
        }
        return response()->json([
            "msg" => " Operación exitosa!",
        ], 200);
        //End foreach
    }


    /**
     * Object the specified resource in storage.
     * @param  string $data
     */

    public function addRevokeUser(Request $request){

        $get = explode("|", $request["data"]);
        $id = str_replace("crypt_id_permisssions","",Crypt::decrypt($get[1]));

        //Verifico  que sesa un permiso especial.
        if($id > 4 && $request["action"] == "assign")
        {
           //Verifico que el usuario no lo tenga asignado todavia.
           $rs = DB::table('permission_user')->where('permission_id', $id)->where('user', $get[0])->first();

            if(sizeof($rs) > 0)
           {
               $code = "-1";
               $msg = "Este permiso ya está asignado al usuario!";
           }else{
               $permission = new PermissionsUser();
               $permission->permission_id = $id;
               $permission->user = $get[0];
               $permission->save();

               $code = "1";
               $msg = "Permiso asignado satisfactoriamente!";
           }
        }
        if($id > 4 && $request["action"] == "revoke")
        {
            $deleted = DB::table('permission_user')->where('permission_id', $id)->where('user', $get[0])->delete();

            if($deleted)
            {
                $code = "1";
                $msg = "Permiso removido satisfactoriamente!";
            }else{
                $code = "-1";
                $msg = "No se ha podido remover el permisos!";
            }
        }

        return response()->json([
            "code"   => $code,
            "msg"    =>  $msg,
        ], 200);
    }

    public function checkAdmin()
    {
        return response()->json([
            "admin"   => User::isAdmin(),
        ], 200);
    }

    public function validateRequest()
    {
       // dd($this->getRouter()->current()->uri());
        dd(User::canMake("roles/create"));
    }

    public static function getActionsByView($view)
    {
        $permissions =  User::getPermissionsByView($view);
        if( count($permissions) > 0)
        {
            $code = 200;
        }else{
            $code = 401;
        }
        return response()->json([
            "code"    => $code,
            "permissions"   => $permissions,
        ], 200);
    }
}
