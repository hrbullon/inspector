<?php

namespace App\Http\Controllers;

use App\Agreements;
use App\Renewals;
use App\Taxes;
use DB;
use Crypt;
use App\AgreementsCompanies;
use App\AgreementsCompaniesDetail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AgreementsCompaniesForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class AgreementsCompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","agreements_companies.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","agreements_companies.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = AgreementsCompanies::search([$request->get("search")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_agreements_companies"),
                "company"=>$values->company,
                "seller"=>$values->firstname. " ".$values->lastname ,
                "date"=>date("d-m-Y", strtotime($values->date)),
                "amount"=>$values->amount,
                "mstatus"=> $values->m_status,
                "name_row"=>'agreements_companies_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //AgreementsCompaniesForm
    public function store(Request $request)
    {
        //Guardo  en la tabla de contratos
        $AgreementsCompanies = new AgreementsCompanies($request->all());
        $AgreementsCompanies->company = $request["company"]["id"];
        $AgreementsCompanies->seller = Session::get("user");//ID DEL USUARIO LOGUEADO
        $AgreementsCompanies->date = date("Y-m-d");
        $AgreementsCompanies->amount = 0.00;
        $AgreementsCompanies->m_status = 7;

        if($AgreementsCompanies->save())
        {
            $AgreementsCompanies->addDetails($request,$AgreementsCompanies->id);
        }

        return response()->json([
            "msg" => "success",
            "id"  => Crypt::encrypt($AgreementsCompanies->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $decrypt = Crypt::decrypt($id);
        $decrypt = str_replace("crypt_id_agreements_companies","",$decrypt);
        $agreement = AgreementsCompanies::find($decrypt);

        if (sizeof($agreement) > 0) {
            //retorna un array y en la posicion 1 viene el impuesto
            $tax = Taxes::getTax($agreement->amount);

            $data = [
                'agreement' => [
                    'number' => $agreement->id,
                    'frecuency' => $agreement->frecuencies->description,
                    'tope_customers'=> $agreement->tope_n_customers,
                    'type_sale' => $agreement->sales->description,
                    'seller' => $agreement->sellers->firstname. " ".$agreement->sellers->lastname,
                    'status' => $agreement->m_status,
                    'amount' => number_format($agreement->amount,2,",","."),
                    'tax' => number_format($tax[1],2,",","."),
                    'total' => number_format(($agreement->amount + $tax[1]),2,",",".")
                ],
                'company'=>[
                    'identity' => $agreement->companies->c_identity,
                    'name' => $agreement->companies->name,
                    'country' => $agreement->companies->countries->name,
                    'state' => $agreement->companies->states->name,
                    'town' => $agreement->companies->towns->name,
                    'address' => $agreement->companies->address,
                    'm_status' => $agreement->companies->m_status
                ],
                'packages'=> AgreementsCompaniesDetail::getServicesPackage($decrypt)
            ];
            $code = 200;
        } else {
            $code = 404;
        }

        return response()->json([
            "data" => $data,
        ], $code);
    }

    /**
     * Anull the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($agreement = AgreementsCompanies::find($decrypt)){
            $agreement->m_status = 104;
            $agreement->save();
            $msg = "success";
            $code = 200;
        }else{
            $msg = "failed";
            $code = 404;
        }
        return response()->json([
            "msg"    =>  $msg,
            "code"   =>  $code
        ], $code);
    }


    public function agreementPdf($id)
    {
        $decrypt = Crypt::decrypt($id);
        $model = AgreementsCompanies::find($decrypt);
        return \PDF::loadView('reports.agreement_company', $model)->download('contrato-'.$model->id.'.pdf');
    }

    public function getAgreements(Request $request)
    {
        $register = "";
        switch ($request['type'])
        {
            case 88:
                $register = AgreementsCompanies::getAgreementsCompany($request["customer"]);


                break;
            case 89:
                $register = Agreements::getAgreementsCustomer($request["customer"]);


                break;
            case 90:
                $register = Renewals::getRenewalsCompany($request["customer"]);

                break;
            case 91:
                $register = Renewals::getRenewalsCustomer($request["customer"]);

                break;
        }
        return response()->json($register);
    }
}
