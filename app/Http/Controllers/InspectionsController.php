<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Crypt;
use App\Inspections;
use App\Http\Requests;
use App\Http\Requests\InspectionsForm;
use App\Http\Controllers\Controller;

class InspectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","inspections.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","inspections.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Inspections::search([$request->get("search"),$request->get("type"), $request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_inspections"),
                "customer"=>$values->first_name.' '.$values->first_surname,
                "user"=>$values->firstname.' '.$values->lastname,
                "vehicle"=>$values->vehicle,
                "date_attended"=>date('d-m-Y', strtotime($values->date_attended)),
                "hora"=>date('H:i:s', strtotime($values->date_attended)),
                "type"=>$values->type,
                "name_row"=>''.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InspectionsForm $request)
    {
        $inspections = new Inspections($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $inspection = Inspections::find($id);

        if (sizeof($inspection) > 0) {
            $inspection->id = $id;
            $type = $inspection->type;
            $msg = 'success';
            $code = 200;
        } else {
            $type = null;
            $msg = 'error';
            $code = 404;
        }
        return response()->json([
            'msg' => $msg,
            'inpection' => $inspection,
            'fecha' => date('d-m-Y',strtotime($inspection->date_attended)),
            'hora' => date('H:i:s',strtotime($inspection->date_attended)),
            'customer' => ($type == 78)? DB::table('customers')->join('companies', 'companies.id', '=', 'company')->where('customers.id', $inspection->customer)->get(["first_name","first_surname","contact_person","companies.address","companies.alias","companies.name"]) : [],
            'files' => ($type == 78)? DB::table('inspections_files')->where('inspection', $id)->get() : [],
            'files_zone' => ($type == 79)? DB::table('inspections_file_zone')->where('inspection', $id)->get() : [],
            'watchers' => ($type == 78)? DB::table('watchers_inspections')->join('watchers', 'watchers.id', '=', 'watcher')->join('security_companies', 'security_companies.id', '=', 'security_company')->where('inspection', $id)->get(["full_name","document_id","name","observation"]) : [],
            'vehicle' => ($type == 78)? DB::table('vehicle')->join('brand', 'brand.id', '=', 'brand')->join('model', 'model.id', '=', 'model')->where('vehicle.id', $inspection->vehicle)->get(["model.name as modelo","brand.name as marca ","plate"]) : [],
            'alarm_count' => ($type == 80)? DB::table('alarm_count')->where('id', $inspection->alarm_count)->get() : [],
            'planning' => ($type == 79)? DB::table('planning')->where('id', $inspection->planning)->get() : [],
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
