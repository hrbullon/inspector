<?php

namespace App\Http\Controllers;

use Crypt;
use App\Currency;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CurrencyForm;
use App\Http\Controllers\Controller;


class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","currency.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","currency.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Currency::search([$request->get("search"),$request->get("country")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_currency"),
                "country"=>$values->country,
                "name"=>$values->name,
                "plural"=>$values->plural,
                "abbreviated"=>$values->abbreviated,
                "exchange_purchase"=>$values->exchange_purchase,
                "exchange_sale"=>$values->exchange_sale,
                "name_row"=>str_replace(" ", "_", $values->name).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurrencyForm $request)
    {
        $currency = Currency::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  => $currency->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $currency = Currency::find($decrypt);
        if (sizeof($currency) > 0) {
            $currency->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "currency" => $currency,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurrencyForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $currency = Currency::find($decrypt);
        $currency->fill($request->all());
        $currency->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($currency = Currency::find($decrypt)){
            $currency->delete();
            $msg = "Moneda eliminada satisfactoriamente";
        }else{
            $msg = "Error, Moneda no encontrada!";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Currency::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }
}
