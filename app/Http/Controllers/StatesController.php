<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\States;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StatesForm;
use App\Http\Controllers\Controller;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","states.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","states.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = States::search([$request->get("search"),$request->get("country"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_state"),
                "country"=>$values->country,
                "name"=>$values->name,
                "name_row"=>str_replace(" ", "_", $values->name),
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatesForm $request)
    {
        $state = States::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  => $state->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $state = States::find($decrypt);
        if (sizeof($state) > 0) {
            $state->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "state" => $state,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StatesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $state = States::find($decrypt);
        $state->fill($request->all());
        $state->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($state = States::find($id)){
            $state->delete();
            $msg = "Role eliminado satisfactoriamente";
        }else{
            $msg = "Error, Estado no encontrado!";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            States::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request)
    {
        if(isset($request["country"]) && !empty($request["country"]) )
        {
            return response()->json(DB::table('states')->where("country",$request["country"])->get(["id", "name"]));
        }else{
            return response()->json(DB::table('states')->get(["id", "name"]));
        }
    }
}
