<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\Banks;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\BanksForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class BanksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","banks.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","banks.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Banks::search([$request->get("search"),$request->get("country")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_banks"),
                "country"=>$values->country,
                "name"=>$values->name,
                "short_name"=>$values->short_name,
                "prefix"=>$values->prefix,
                "web_page"=>$values->web_page, 
                "master_phone"=>$values->master_phone, 
                "conformation_phone"=>$values->conformation_phone
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BanksForm $request)
    {
        $bank = Banks::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($bank->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $bank = Banks::find($decrypt);
        if (sizeof($bank) > 0) {
            $bank->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "bank" => $bank,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BanksForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $bank = Banks::find($decrypt);
        $bank->fill($request->all());
        $bank->save();

        return response()->json([ "msg"    =>  "success", ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($bank = Banks::find($decrypt)){
            $bank->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([ "msg"    =>  $msg, ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Banks::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request){
        if(isset($request["country"]) && !empty($request["country"])) {
            return response()->json(DB::table('banks')->where("country","=",$request["country"])->get(["id", "name"]));
        }
        else{
            return response()->json(DB::table('banks')->get(["id", "name"]));
        }
    }

    public function getPrefix(Request $request){
        if(isset($request["id"]) && !empty($request["id"]) ) {
            return response()->json(DB::table('banks')->where("id","=",$request["id"])->get(["prefix"]));
        }
    }

    public function getCountry(Request $request){
        if(isset($request["id"]) && !empty($request["id"]) ) {
            return response()->json(DB::table('banks')->where("id","=",$request["id"])->get(["country"]));
        }
    }

    public function getBanks(Request $request)
    {
            return response()->json(DB::table('banks')->where("country","=",Session::get('country'))->get(["id","name"]));
    }

}