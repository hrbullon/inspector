<?php

namespace App\Http\Controllers;

use Crypt;
use App\Services;
use App\UsersCompany;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ServicesForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","services.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","services.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Services::search([$request->get("search"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_services"),
                "inning"=>$values->inning,
                "name"=>$values->name,
                "number_visit"=>$values->number_visit,
                "hour_from"=>$values->hour_from,
                "hour_to"=>$values->hour_to,
                "price"=>$values->amount,
                "mstatus"=>$values->mstatus,
                "name_row"=>str_replace(" ","_",$values->name).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $service = new Services($request->all());
        $service->company = Session::get("company");
        $service->save();

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($service->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $services = Services::find($decrypt);
        if (sizeof($services) > 0) {
            $services->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }

        return response()->json([
            "msg" => $msg,
            "service" => $services,
        ], $code);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $service = Services::find($decrypt);
        $service->fill($request->all());
        $service->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($service = Services::find($decrypt)){
            $service->mstatus = 0;
            $service->save();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            $service = Services::find($decrypt);
            $service->mstatus = 0;
            $service->save();
            $flag = true;
        }

        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getServices()
    {
        return response()->json(\DB::table('services')->where('company',1)->get(["id","name","inning","number_visit","amount"]));
    }
}