<?php

namespace App\Http\Controllers;

use App\MailAccountCompany;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Transport\Transport;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\Facades\App;

use Mail;

class EmailsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Html
     */
    public function inspection()
    {
        if(MailAccountCompany::setParams())
        {

            Mail::send('emails.inspeccionesnuevos', ["test"=>"test"], function ($message){
                $message->subject('Reporte de Inspección - CLIENTE');
                $message->from('hrbullon@sagasoftware.net', 'Accion Inmediata, C.A');
                $message->to('the_father_573@hotmail.com');
                $message->cc("hrbullon@gmail.com");
            });
        }
        return view("emails.inspeccionesnuevos");
    }
}
