<?php

namespace App\Http\Controllers;

use App\ScheduleSettings;
use App\User;
use Illuminate\Http\Request;

use Crypt;
use App\UsersSchedules;
use App\Http\Requests\UsersSchedulesForm;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class UsersSchedulesController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json(["events"  => UsersSchedules::getEvents()],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersSchedulesForm $request)
    {
        $usersSchedule = new UsersSchedules($request->all());
        $usersSchedule->from = date("Y-m-d", strtotime($request["from"]));
        $usersSchedule->end = (isset($request["end"]) && $request["end"] !=="")? date("Y-m-d", strtotime($request["end"])) : "";
        $code = ($usersSchedule->save())? 200 : 400;
        return response()->json(["id"  =>  Crypt::encrypt($usersSchedule->id)],$code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersSchedulesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $usersSchedule = UsersSchedules::find($decrypt);
        $usersSchedule->fill($request->all());
        $usersSchedule->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        $usersSchedule = UsersSchedules::find($decrypt);
        $usersSchedule->delete();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    public function getDataInit()
    {
        $users = User::getUserByType("inspector");
        $inning = ScheduleSettings::all()->where("company",2);
        return response()->json(["users"=>$users, "inning" => $inning]);
    }
}
