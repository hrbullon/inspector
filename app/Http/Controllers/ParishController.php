<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\Parish;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ParishForm;
use App\Http\Controllers\Controller;


class ParishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","parish.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","parish.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Parish::search([$request->get("search")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_parish"),
                "town"=>$values->town,
                "name"=>$values->name,
                "name_row"=>str_replace(" ", "_", $values->name),
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParishForm $request)
    {
        $parish = Parish::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  => $parish->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $parish = Parish::find($decrypt);
        if (sizeof($parish) > 0) {
            $parish->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "parish" => $parish,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ParishForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $parish = Parish::find($decrypt);
        $parish->fill($request->all());
        $parish->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($parish = Parish::find($decrypt)){
            $parish->delete();
            $msg = "Parroquia eliminada satisfactoriamente";
        }else{
            $msg = "Error, Parroquia no encontrado!";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Parish::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request)
    {
        return response()->json(DB::table('parish')->where("town",$request["town"])->get(["id", "name"]));
    }

}
