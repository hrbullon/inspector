<?php

namespace App\Http\Controllers;

use App\ActivedAlarms;
use App\AlarmCount;
use App\Inspections;
use App\CommentsActivation;
use App\ActivedAlarmsZonning;
use Illuminate\Http\Request;

use DB;
use Crypt;
use App\Http\Requests;
use App\Http\Requests\ActivedAlarmsForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ActivedAlarmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","actived_alarms.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","actived_alarms.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = ActivedAlarms::search([$request->get("search"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                'id'=>Crypt::encrypt($values->id.'crypt_id_actived_alarms'),
                'account'=>$values->account,
                'customer'=>$values->first_name .' '.$values->first_surname,
                'activation_date'=> date('d-m-Y H:i:s', strtotime($values->activation_date)),
                'created_by'=>$values->firstname.' '.$values->lastname,
                'mstatus'=>$values->mstatus,
                "name_row"=>'alarmcount_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *///ActivedAlarmsForm
    public function store(ActivedAlarmsForm $request)
    {
        //user id debe ser tomado d e la session activa
        $activation = new ActivedAlarms($request->all());
        $activation->created_by = Session::get('user');
        $activation->created_date = date('Y-m-d H:i:s');
        $activation->mstatus = 38;
        //En caso de que guarde correctamente la activacion,
        //inserto zonas activadas y comentarios.
        if($activation->save())
        {
            ActivedAlarmsZonning::InsertZoning($activation, $request);
            CommentsActivation::InsertComments($activation, $request, $activation->created_by);
            //Obtengo el id del cliente en cuestión.
            $account = AlarmCount::find($activation->account);
            //Ahora inserto el notification en inspection
            $inspection = Inspections::InsertInspection($activation->id, $account->customer);
            //Ahora con el id de la inpseccion puedo  guardar los archivos.

        }

        return response()->json([
            'msg' => 'success',
            'id'  => Crypt::encrypt($activation->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        //$decrypt = $id;
        $activation = ActivedAlarms::find($decrypt);

        if (sizeof($activation) > 0) {
            $activeIde = $activation->id;
            $activation->id = Crypt::encrypt($id."crypt_id_actived_alarms");
            
            return response()->json([
                'msg' => 'success',
                'activation' => $activation,
                'comments' =>  CommentsActivation::GetComments($activeIde),
                'zoning' => ActivedAlarmsZonning::GetZoning($activeIde),
                'account' => AlarmCount::where("id",$activation->account)
                    ->get(['account','name', 'latitude', 'longitude', 'address'])
            ], 200);
        } else {
            return response()->json([
                'msg' => 'error',
            ], 400);
        }
    }

    /**
     * Update info to Activation
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addRemoveZoning(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $activation = ActivedAlarms::find($decrypt);

        if(count($activation) > 0)
        {
            if($request['checked'] == 'true')
            {
                $zoning =  \DB::table('actived_alarms_zoning')
                    ->where('activation',$activation->id)->where('zoning',$request['zoning']);
                if(count($zoning) !== null){
                    $activeZoning = new ActivedAlarmsZonning();
                    $activeZoning->activation = $activation->id;
                    $activeZoning->zoning = $request['zoning'];
                    $activeZoning->save();
                }
            }elseif ($request['checked'] == 'false'){
                \DB::table('actived_alarms_zoning')
                    ->where('activation',$activation->id)->where('zoning',$request['zoning'])->delete();
            }
            return response()->json([
                "msg"    =>  "success",
            ], 200);
        }else{
            return response()->json([
                "msg"    =>  "error",
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $activation = ActivedAlarms::find($decrypt);

        if(count($activation) > 0 && ($request['mstatus'] == 39 || $request['mstatus'] == 40 || $request['mstatus'] == 81))
        {
            $activation->mstatus = $request['mstatus'];
            $activation->save();

            $msg = 'success';
            $code = 200;
        }else{
            $msg = 'error';
            $code = 404;
        }

        return response()->json([
            "msg"    =>  $msg,
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $activation = ActivedAlarms::find($decrypt);

        if(count($activation) > 0 )
        {
            $comment = new CommentsActivation();
            $comment->activation = $activation->id;
            $comment->operator = Session::get('user');
            $comment->comment = $request['comment'];
            $comment->date = date('Y-m-d H:i:s');
            $comment->save();

            $msg = 'success';
            $code = 200;
        }else{

            $msg = 'error';
            $code = 404;
        }

        return response()->json([
            "msg"    =>  $msg,
        ], $code);

    }

    public function addFiles($id)
    {

    }

    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($activedAlarms = ActivedAlarms::find($decrypt)){
            $activedAlarms->mstatus = 81;
            $activedAlarms->save();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function autocomplete(Request $request){
        return response()->json(DB::table('alarm_count')->where("name","LIKE","%".$request["search"]."%")->orWhere("account","LIKE","%".$request["search"]."%")->get(array('id', DB::raw('CONCAT(account, " ", name) as text'))));
    }

    public function getZones(Request $request){
        return response()->json(DB::table('zoning_alarm')
                        ->select("zoning_alarm.name", "zoning_alarm.mstatus", "alarm_count.address", "alarm_count.latitude", "alarm_count.longitude")
                        ->join('alarm_count', 'zoning_alarm.alarm_count', '=', 'alarm_count.id')
                        ->where("zoning_alarm.alarm_count", $request["id"])
                        ->get());
    }

    public function getInstallation(Request $request){
        if(isset($request["id"]) && !empty($request["id"]) ){
            return response()->json(DB::table('alarm_count')->where("id", $request["id"])->get(["account", "address", "latitude", "longitude"]));
        }
    }

    public function getOperator(Request $request){
        return response()->json(DB::table('users')->where("id", Session::get('user'))->get(["id","firstname","lastname"]));
        
    }

    public function getArchived(Request $request){
        return response()->json(DB::table('inspections_files')->get());
        
    }
}