<?php

namespace App\Http\Controllers;


use Crypt;

use App\AlarmCount;
use App\ZoningAlarm;
use App\Inspections;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AlarmCountForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use DB;

class AlarmCountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","alarm_count.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","alarm_count.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = AlarmCount::search([$request->get("search"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                'id'=>Crypt::encrypt($values->id.'crypt_id_alarm_count'),
                'customer'=>$values->first_name .' '.$values->first_surname,
                'name'=>$values->name,
                'account'=>$values->account,
                'protocol'=>$values->protocol,
                'mstatus'=>$values->mstatus,
                'name_row'=>'alarmcount_'.$values->id,
            ];
            array_push($data, $array);
        }
        
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlarmCountForm $request)
    {
        /*$acount = new AlarmCount($request->all());
        $acount->company = Session::get('company');*/
        $acount = new AlarmCount();
        $acount->customer = $request['customer'];
        $acount->protocol = $request['protocol'];
        $acount->account = $request['account'];
        $acount->name = $request['name'];
        $acount->country = $request['country'];
        $acount->states = $request['states'];
        $acount->town = $request['town'];
        $acount->parish = $request['parish'];
        $acount->latitude = $request['latitude'];
        $acount->longitude = $request['longitude'];
        $acount->address = $request['address'];
        $acount->mstatus = $request['mstatus'];
        $acount->company = Session::get('company');
        //En caso de que guarde correctamente la instalación.
        //inserto zonas para la cuenta recien creada
        if($acount->save())
        {
            //Las zonas a insertar deben ser enviadas en un array bidimencional de esta manera.
            $zonas = $request['zoning'];
            if(count($zonas)>0)
            {
                //Inserto las zonas que se hayan activado
                for($x=0;$x<count($zonas);$x++)
                {
                    $accountZone = new ZoningAlarm();
                    $accountZone->alarm_count = $acount->id;
                    $accountZone->zone = $zonas[$x]['zone'];
                    $accountZone->name = $zonas[$x]['name'];
                    $accountZone->mstatus = $zonas[$x]['mstatus'];
                    $accountZone->save();
                }
            }

            //Ahora inserto el notification en inspection
            //Por ahora inoperativo
            $inspection = new Inspections();
            $inspection->customer = $acount->customer;
            $inspection->type = 80;
            $inspection->alarm_count = $acount->id;
            $inspection->user = Session::get('user');
            $inspection->date_created = date('Y-m-d H:i:s');
            $inspection->save();
        }

        return response()->json([
            'msg' => 'success',
            'id'  => Crypt::encrypt($acount->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $acount = AlarmCount::find($decrypt);

        if (sizeof($acount) > 0) {
            $idAccount = $acount->id;
            $acount->id = $id;
            $msg = 'success';
            $code = 200;
        } else {
            $msg = 'error';
            $code = 404;
        }
        return response()->json([
            'msg' => $msg,
            'account' => $acount,
            /*'zoning' => ZoningAlarm::where("alarm_count",$idAccount)
                ->get(['id','zone', 'name', 'mstatus'])*/
            
            'zoning' => ZoningAlarm::where("alarm_count",$idAccount)
            ->join('status', 'status.value', '=', 'mstatus')
            ->where("status.source","26")
            ->get(['zoning_alarm.id','zoning_alarm.zone', 'zoning_alarm.name', 'zoning_alarm.mstatus', 'status.description'])
        ], $code);
    }

    public function addZone(Request $request,$id)
    {

        $decrypt = Crypt::decrypt($id);
        $installation = AlarmCount::find($decrypt);

        if(count($installation) > 0 )
        {
            $comment = new ZoningAlarm();
            $comment->alarm_count = $installation->id;
            $comment->zone = $request['zone'];
            $comment->name = $request['name'];
            $comment->mstatus = $request['mstatus'];
            $comment->save();

            $msg = 'success';
            $code = 200;
        }else{
            $msg = 'error';
            $code = 404;
        }

        return response()->json([
            "msg"    =>  $msg,
        ], $code);

    }

    public function destroyZone($id)
    {
        if($zone = ZoningAlarm::find($id)){
            $zone->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlarmCountForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $acount = AlarmCount::find($decrypt);
        $acount->fill($request->all());
        $acount->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateZone(Request $request, $id)
    {
        $zoning = ZoningAlarm::find($request['id']);
        if(count($zoning) > 0)
        {
            $zoning->fill($request->all());
            $zoning->save();

            $msg = 'success';
            $code = 200;
        }else{
            $msg = 'error';
            $code = 404;
        }

        return response()->json([
            "msg"    =>  $msg,
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($installation = AlarmCount::find($decrypt)){
            $installation->mstatus = 12;
            $installation->save();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]);
            $installation = AlarmCount::find($decrypt);
            $installation->mstatus = 12;
            $installation->save();
            $flag = true;
        }

        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }
}