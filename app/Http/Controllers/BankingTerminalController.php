<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Crypt;
use App\BankingTerminal;
use App\Http\Requests;
use App\Http\Requests\BankingTerminalForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BankingTerminalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","banking_terminal.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","banking_terminal.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = BankingTerminal::search([$request->get("search"),$request->get("banks"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_banks"),
                "bank_Account"=> $values->name . ' ' . $values->account,
                "number"=>$values->number,
                "description"=>$values->description,
                "m_status"=>$values->m_status,
                "name_row"=>str_replace(" ", "_",$values->name . ' ' . $values->account),
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankingTerminalForm $request)
    {
        $bankTerminal = new BankingTerminal($request->all());
        $bankTerminal->company = 16;//Session::get("company")
        $bankTerminal->save();

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($bankTerminal->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $bankTerminal = BankingTerminal::find($decrypt);
        if (sizeof($bankTerminal) > 0) {
            $bankTerminal->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "bankTerminal" => $bankTerminal,
        ], $code);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $bankTerminal = BankingTerminal::find($decrypt);
        $bankTerminal->fill($request->all());
        $bankTerminal->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($bankTerminal = BankingTerminal::find($decrypt)){
            $bankTerminal->delete();
            $msg = "success";
            $code = 200;
        }else{
            $msg = "failed";
            $code = 404;
        }
        return response()->json([
            "msg"    =>  $msg,
            "code"   =>  $code
        ], $code);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]);
            $service = BankingTerminal::find($decrypt);
            $service->m_status = 103;
            $service->save();
            $flag = true;
        }

        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getTerminals()
    {
        //return response()->json(\DB::table('banking_terminal')->where("company","=",Session::get('company'))->get(["id","number","description"]));
    }
}
