<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Crypt;
use App\ScheduleSettings;
use App\Http\Requests;
use App\Http\Requests\ScheduleSettingsForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ScheduleSettingsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleSettingsForm $request)
    {
        $scheduleSettings = new ScheduleSettings($request->all());
        $scheduleSettings->company = Session::get("company");
        if($scheduleSettings->handles_morning == 0){
            $scheduleSettings->morning_beggining = "";
            $scheduleSettings->morning_finish = "";
        }else{
            $scheduleSettings->morning_beggining = date("h:i a", strtotime($scheduleSettings->morning_beggining));
            $scheduleSettings->morning_finish = date("h:i a", strtotime($scheduleSettings->morning_finish));
        }
        if($scheduleSettings->handles_evening == 0){
            $scheduleSettings->evening_beggining = "";
            $scheduleSettings->evening_finish = "";
        }else{
            $scheduleSettings->evening_beggining = date("h:i a", strtotime($scheduleSettings->evening_beggining));
            $scheduleSettings->evening_finish = date("h:i a", strtotime($scheduleSettings->evening_finish));
        }
        if($scheduleSettings->handles_night == 0){
            $scheduleSettings->night_beggining = "";
            $scheduleSettings->night_finish = "";
        }else{
            $scheduleSettings->night_beggining = date("h:i a", strtotime($scheduleSettings->night_beggining));
            $scheduleSettings->night_finish = date("h:i a", strtotime($scheduleSettings->night_finish));
        }

        $code = ($scheduleSettings->save())? 200 : 400;
        return response()->json(["id"  =>  Crypt::encrypt($scheduleSettings->id)],$code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Session::get("company");
        $scheduleSettings = \DB::table("schedule_settings")->where("company",2)->get();
        if(sizeof($scheduleSettings) > 0)
        {
            $code = 200;
            $message = "Completado";
            $scheduleSettings[0]->id = Crypt::encrypt($scheduleSettings[0]->id);
        }else{
            $code = 404;
            $message = "No encontrado";
        }
        return response()->json(['msg'=>$message, 'schedules_settings'=> $scheduleSettings], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleSettingsForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $scheduleSettings = ScheduleSettings::find($decrypt);
        if(sizeof($scheduleSettings) > 0)
        {
            $scheduleSettings->fill($request->all());
            if($scheduleSettings->handles_morning == 0){
                $scheduleSettings->morning_beggining = "";
                $scheduleSettings->morning_finish = "";
            }
            if($scheduleSettings->handles_evening == 0){
                $scheduleSettings->evening_beggining = "";
                $scheduleSettings->evening_finish = "";
            }
            if($scheduleSettings->handles_night == 0){
                $scheduleSettings->night_beggining = "";
                $scheduleSettings->night_finish = "";
            }
            if($scheduleSettings->save())
            {
                $code = 200;
                $message = "Actualizado";
            }else{
                $code = 400;
                $message = "Error en petición";
            }
        }else{
            $code = 404;
            $message = "No encontrado";
        }
        return response()->json(["msg"=>$message], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyItems(Request $request)
    {
        $flag = false;
        if(sizeof($request["items"]) > 0)
        {
            for ($x = 0; $x < count($request["items"]); $x++) {
                $decrypt = Crypt::decrypt($request["items"][$x]["id"]);
                ScheduleSettings::find($decrypt)->delete();
                $flag = true;
            }
            $code = 200;
        }else{
            $code = 400;
        }
        return response()->json(["flag"   =>  $flag], $code);
    }

    
}
