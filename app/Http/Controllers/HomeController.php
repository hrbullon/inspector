<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        /*dd(\DB::table("user_company")
            ->select("companies.id as company","countries.id as country")
            ->join('companies', 'user_company.company', '=', 'companies.id')
            ->join('countries', 'companies.country', '=', 'countries.id')
            ->where("user",Auth::user()->id)->get());*/

        return view("welcome");
    }

    /**
     * Bloquea la session del usuario
     * @return view
     */
    public function lockscreen()
    {
        return view("auth.lockscreen");
    }

    public function unlockscreen(Request $request)
    {

        dd(Auth::user());

        //echo Auth::user()->password ." -------------". bcrypt($request["password"]);

        /*if(Auth::user()->password == bcrypt($request["password"]))
        {
            return Redirect::to("/");
        }else{
            return redirect()->back()->withInput()->withErrors(["error"=>"Contraseña invalida"]);
        }*/
    }
}
