<?php

namespace App\Http\Controllers;

use App\ScheduleSettings;
use App\User;
use Crypt;
use App\Roles;
use Illuminate\Http\Request;
use App\Http\Requests\RolesForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","roles.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","roles.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Roles::search([$request["search"]])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_roles"),
                "name"=>$values->name,
                "slug"=>$values->slug,
                "description"=>$values->description,
                "level"=>$values->level,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesForm $request)
    {
        $role = new Roles($request->all());
        $role->company = Session::get("company");
        $role->save();

        return response()->json([
            "msg" => "success",
            "id"  =>  Crypt::encrypt($role->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $role = Roles::find($decrypt);
        if (sizeof($role) > 0) {
            $role->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "role" => $role,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $role = Roles::find($decrypt);
        $role->fill($request->all());
        $role->level = $request["level"];
        $role->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        if(sizeof($request["items"]) > 0)
        {
            for ($x = 0; $x < count($request["items"]); $x++) {
                $decrypt = Crypt::decrypt($request["items"][$x]["id"]);
                Roles::find($decrypt)->delete();
                $flag = true;
            }
            $code = 200;
        }else{
            $code = 400;
        }
        return response()->json(["flag"   =>  $flag], $code);
    }


    public function getRoles(Request $request)
    {
        if(User::isAdmin())
        {
            $roles = Roles::all(["id","name"]);
        }else{
            $roles = \DB::table("roles")->where("level",$request->session()->get("data")->type_identity)->get(["id","name"]);
        }
        return $roles;
    }
}
