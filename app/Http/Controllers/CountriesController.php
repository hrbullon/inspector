<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\Countries;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CountriesForm;
use App\Http\Controllers\Controller;


class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","countries.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","countries.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Countries::search([$request->get("search")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_countries"),
                "name"=>$values->name,
                "area_code"=>$values->area_code,
                "icon"=>$values->icon,
                'name_row'=>str_replace(" ", "_", $values->name.''.$values->area_code)
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountriesForm $request)
    {
        $country = Countries::create($request->all());
        return response()->json([
            "msg" => "success",
            "id"  => $country->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $country = Countries::find($decrypt);
        if (sizeof($country) > 0) {
            $country->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "country" => $country
        ,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountriesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $country = Countries::find($decrypt);
        $country->fill($request->all());
        $country->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($country = Countries::find($id)){
            $country->delete();
            $msg = "País eliminado satisfactoriamente";
        }else{
            $msg = "Error, país no encontrado!";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Countries::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }

    public function getComboBox(Request $request)
    {
        return response()->json(DB::table('countries')->get(["id", "name", "icon"]));
    }

    public function getRegexPhone(Request $request)
    {
        return response()->json(DB::table('countries')
                ->select("countries.format_phone")
                ->where("countries.id", $request['country'])
                ->get());
    }
}
