<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Crypt;
use App\Views;
use App\Http\Requests;
use App\Http\Requests\ViewsForm;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class ViewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response3
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","view.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","view.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Views::search([$request["search"],$request["status"]])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_views"),
                "name"=>$values->name,
                "action"=>$values->action,
                "icon"=>$values->icon,
                "position"=>$values->position,
                "mstatus"=>$values->mstatus,
                "name_row"=>str_replace(" ", "_", $values->name).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ViewsForm $request)
    {
        $view = new Views($request->all());
        if(($request["parent"] > 0))
        {
            $view->level = 2;
            $view->position = 0;
        }else{
            $view->level = 1;
            //Asigna automaticamente en la ultima posición el nuevo menu...
            $view->position = DB::table('view')->where('parent', 0)->max("position")+1;
        }
        $view->save();
        return response()->json([
            "msg" => "success",
            "id"  => $view->id
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $view = Views::find($decrypt);
        if (sizeof($view) > 0) {
            $view->id = $id;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "menu" => $view,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $view = Views::find($decrypt);
        $view->fill($request->all());
        $view->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($view = Views::find($decrypt)){
            $view->delete();
            $msg = "success";
        }else{
            $msg = "failed";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Views::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }


    public function getParents()
    {
        return DB::table('view')->where('parent',0)->get(["id","name"]);
    }

    public function GetIcons($search)
    {
        $fileNM = "assets/global/css/icons/font-awesome/font-awesome.css";
        $icons = array();
        $count = 0;
        if (file_exists($fileNM)) {
            $gestor = @fopen($fileNM, "r");

            if ($gestor) {
                $nl = 0;
                while (($buffer = fgets($gestor, 4096)) !== false) {
                    if ($nl >= 186) {
                        $line = explode(':', $buffer);
                        $name = trim(str_replace('.fa-', '', $line[0]));
                        if ($name != "content" && $name != "}") {
                            if(strstr($name, $search) == TRUE && $count < 8){
                                $icons[$name] = $name;
                                $count++;
                            }
                        }
                    }
                    $nl++;
                }
                fclose($gestor);
                sort($icons);
            }
        }
        return response()->json([
            "msg"    =>  "success",
            "icons"  =>  $icons
        ], 200);
    }

    public function GetIconos()
    {
        $fileNM = "assets/global/css/icons/font-awesome/font-awesome.css";
        $icons = array();
        $count = 0;
        if (file_exists($fileNM)) {
            $gestor = @fopen($fileNM, "r");

            if ($gestor) {
                $nl = 0;
                while (($buffer = fgets($gestor, 4096)) !== false) {
                    if ($nl >= 186) {
                        $line = explode(':', $buffer);
                        $name = trim(str_replace('.fa-', '', $line[0]));
                        if ($name != "content" && $name != "}") {
                           
                                $icons[$name] = $name;
                                $count++;
                            
                        }
                    }
                    $nl++;
                }
                fclose($gestor);
                sort($icons);
            }
        }
        return response()->json([
            "msg"    =>  "success",
            "iconos"  =>  $icons
        ], 200);
    }

    public function getActions($search)
    {
        return response()->json(DB::table('view')->where("action","=",$search)->get(["action"]));
    }

}
