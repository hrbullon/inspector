<?php

namespace App\Http\Controllers;

use DB;
use Crypt;
use App\Taxes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\TaxesForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class TaxesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if(preg_match("/-/",$request->get("order")))
        {
            $order = str_replace("-","","taxes.".$request->get("order"));
            $sort  = "desc";
        }else{
            $order = str_replace(" ","","taxes.".$request->get("order"));
            $sort  = "asc";
        }
        $rs = Taxes::search([$request->get("search"),$request->get("country"),$request->get("status")])->orderBy($order,$sort)->paginate($request["per_page"]);

        foreach($rs as $values)
        {
            $array = [
                "id"=>Crypt::encrypt($values->id."crypt_id_taxes"),
                "country"=>$values->country,
                "name"=>$values->name,
                "aliquot"=>$values->aliquot,
                "m_status"=>$values->m_status,
                "applicable_from"=> $values->applicable_from/*date("d-m-Y", strtotime($values->applicable_from))*/,
                "name_row"=>str_replace(" ", "_", $values->country).'_'.$values->id,
            ];
            array_push($data, $array);
        }
        $from = ($rs->currentPage()*$rs->perPage()) - $rs->perPage();
        $to = $rs->currentPage() * $rs->perPage();
        return ["current_page"=>$rs->currentPage(),
            "data"=>$data,
            "from"=>($from == 0)? 1 : $from,
            "to"=> ($to > $rs->total())? $rs->total() : $to,
            "last_page"=>$rs->lastPage(),
            "per_page"=>$rs->perPage(),
            "total"=>$rs->total()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxesForm $request)
    {
        $pais = 1; //session variable
        $tax = new Taxes($request->all());
        if($tax->predetermined == 1)
        {
            DB::table('taxes')
                ->where('country', $pais)
                ->update(['predetermined' => 0]);
        }
        $tax->save();

        return response()->json([
            "msg" => "success",
            "id"  => Crypt::encrypt($tax->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $decrypt = Crypt::decrypt($id);
        $tax = Taxes::find($decrypt);
        if (sizeof($tax) > 0) {
            $tax->id = $id;
            $tax->applicable_from = $tax->applicable_from/*date("d-m-Y", strtotime($tax->applicable_from))*/;
            $msg = "success";
            $code = 200;
        } else {
            $msg = "error";
            $code = 404;
        }
        return response()->json([
            "msg" => $msg,
            "tax" => $tax,
        ], $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaxesForm $request, $id)
    {
        $decrypt = Crypt::decrypt($id);
        $tax = Taxes::find($decrypt);
        $tax->fill($request->all());
        $tax->save();

        return response()->json([
            "msg"    =>  "success",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decrypt = Crypt::decrypt($id);
        if($tax = Taxes::find($decrypt)){
            $tax->delete();
            $msg = "Impuesto eliminado satisfactoriamente";
        }else{
            $msg = "Error, Impuesto no encontrado!";
        }
        return response()->json([
            "msg"    =>  $msg,
        ], 200);
    }

    public function destroyItems(Request $request)
    {
        $flag = false;
        for ($x = 0; $x < count($request["items"]); $x++) {
            $decrypt = Crypt::decrypt($request["items"][$x]['id']);
            Taxes::find($decrypt)->delete();
            $flag = true;
        }
        return response()->json([
            "msg"    =>  "success",
            "flag"   =>  $flag
        ], 200);
    }
}
