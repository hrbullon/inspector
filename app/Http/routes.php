<?php

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('lockscreen', ['middleware'=>'auth','uses'=>'HomeController@lockscreen']);
Route::post('unlockscreen', ['middleware'=>'auth','uses'=>'HomeController@unlockscreen']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('/', ['middleware'=>'auth','uses'=>'HomeController@index']);

//'middleware'=>'auth',

// Roles
Route::resource('roles', 'RolesController');
Route::get("getroles","RolesController@getRoles");
Route::get("getroles/{id}","RolesController@getRoles");
Route::post('roles/removeitems', 'RolesController@destroyItems');
Route::get("checkprivilegio","PermissionsController@checkAdmin");

// Banks
Route::resource('banks', 'BanksController');
Route::resource('banks/removeitems', 'BanksController@destroyItems');
Route::post('banks/getcombobox', 'BanksController@getComboBox');
Route::post('banks/getprefix', 'BanksController@getPrefix');
Route::post('banks/getcountry', 'BanksController@getCountry');
Route::post('banks/getbanks', 'BanksController@getBanks');

//Schedule Settings
Route::resource('schedulesettings', 'ScheduleSettingsController');
Route::resource('schedulescalendar', 'UsersSchedulesController');
Route::get('initusersschedules', 'UsersSchedulesController@getDataInit');

// Services Package
Route::resource('servicespackage', 'ServicesPackageController');
Route::post('servicespackage/removeitems', 'ServicesPackageController@destroyItems');

// Countries
Route::resource('countries', 'CountriesController');
Route::post("countries/getcombobox","CountriesController@getComboBox");
Route::post('countries/removeitems', 'CountriesController@destroyItems');
Route::post('countries/getregexphone', 'CountriesController@getRegexPhone');

// Status
Route::resource('status', 'StatusController');
Route::post("status/getcombobox","StatusController@getComboBox");
Route::post("status/getcomboboxgtm","StatusController@getcomboboxGTM");
Route::post("status/getcomboboxvalue","StatusController@getComboBoxValue");

// States
Route::resource('states', 'StatesController');
Route::post('states/getcombobox', 'StatesController@getCombobox');
Route::post('states/removeitems', 'StatesController@destroyItems');

// Town
Route::resource('town', 'TownController');
Route::post('town/removeitems', 'TownController@destroyItems');
Route::post('town/getcombobox', 'TownController@getComboBox');

// Parish
Route::resource('parish', 'ParishController');
Route::post('parish/removeitems', 'ParishController@destroyItems');
Route::post('parish/getcombobox', 'ParishController@getComboBox');

// Services
Route::resource('services', 'ServicesController');
Route::post('services/removeitems', 'ServicesController@destroyItems');
Route::post('getcomboboxServices', 'ServicesPackageController@getComboBox');

// Permissions
Route::resource('permissions', 'PermissionsController');
Route::post('permissions/addrevokeuser', 'PermissionsController@addRevokeUser');
Route::post('permissions/permissionsusers', 'PermissionsController@getPermissionsUsers');
Route::get("getItems","PermissionsController@getItems");
Route::get("getPermission/{id}","PermissionsController@getPermission");
Route::post('permissions/removeitems', 'PermissionsController@destroyItems');

// Users
Route::resource('users', 'UsersController');
Route::post("users/autocomplete","UsersController@autocomplete");
Route::post('users/removeitems', 'UsersController@destroyItems');

// Menus
Route::resource('menus','ViewsController');
Route::get('getParents', 'ViewsController@getParents');
Route::get('geticons/{search}', 'ViewsController@GetIcons');
Route::post('menus/removeitems', 'ViewsController@destroyItems');
Route::post('geticonos', 'ViewsController@GetIconos');
Route::get('menus/getactions/{search}', 'ViewsController@GetActions');

// Currency
Route::resource('currency', 'CurrencyController');
Route::post('currency/removeitems', 'CurrencyController@destroyItems');

// BankAccounts
Route::resource('bankaccounts', 'BankAccountsController');
Route::post('bankaccounts/getaccounts', 'BankAccountsController@getAccount');
Route::post('bankaccounts/removeitems', 'BankAccountsController@destroyItems');

// Taxes
Route::resource('taxes', 'TaxesController');
Route::post('taxes/removeitems', 'TaxesController@destroyItems');

// Banking terminal
Route::resource('bankingterminal', 'BankingTerminalController');
Route::post('bankingterminal/removeitems', 'BankingTerminalController@destroyItems');
Route::post('bankingterminal/getterminal', 'BankingTerminalController@getTerminals');

// Users
Route::resource('users', 'UsersController');
Route::post("users/autocomplete","UsersController@autocomplete");
Route::post('users/removeitems', 'UsersController@destroyItems');

// Paymentmethod
Route::resource('paymentmethod', 'PaymentMethodsController');

// AlarmCount
Route::resource('alarmcount', 'AlarmCountController');
Route::post('alarmcount/addzone/{id}', 'AlarmCountController@addZone');
Route::put('alarmcount/updatezone/{id}', 'AlarmCountController@updateZone');
Route::get('alarmcount/destroyzone/{id}', 'AlarmCountController@destroyZone');
Route::post("alarmcount/autocomplete","AlarmCountController@autocomplete");
Route::post('alarmcount/removeitems', 'AlarmCountController@destroyItems');

// Activations
Route::resource('activations', 'ActivedAlarmsController');
Route::put('activations/zoning/{id}', 'ActivedAlarmsController@addRemoveZoning');
Route::put('activations/changestatus/{id}', 'ActivedAlarmsController@changeStatus');
Route::put('activations/addcomment/{id}', 'ActivedAlarmsController@addComment');
Route::post('activations/removeitems', 'ActivedAlarmsController@destroyItems');
Route::post("activations/autocomplete","ActivedAlarmsController@autocomplete");
Route::post("activations/getzones","ActivedAlarmsController@getZones");
Route::post("activations/getinstallation","ActivedAlarmsController@getInstallation");
Route::post("activations/getoperator","ActivedAlarmsController@getOperator");
Route::post("activations/getarchived","ActivedAlarmsController@getArchived");

// Inspections
Route::resource('inspections', 'InspectionsController');

// Companies
Route::resource('companies', 'CompaniesController');
Route::post("companies/autocomplete","CompaniesController@autocomplete");
Route::post("companies/formatidentity","CompaniesController@getFormatidentity");
Route::post("companies/getcombobox","CompaniesController@getComboBox");
Route::post("companies/searchsecuritycompanies","CompaniesController@searchSecurityCompanies");

// Accountstatus
Route::resource('accountstatus', 'AccountStatusController');
Route::get('customer/accountstatus/{id}', 'AccountStatusController@customer');
Route::get('company/accountstatus/{id}', 'AccountStatusController@company');

// Agreements
Route::resource('agreementscompanies', 'AgreementsCompaniesController');
Route::resource('agreementscustomers', 'AgreementsController');
Route::get('agreements/getdata/{id}', 'AgreementsCompaniesController@getData');
Route::post('agreements/getagreements', 'AgreementsCompaniesController@getAgreements');
Route::get('agreements/customers/pdf/{id}', 'AgreementsController@agreementPdf');
Route::get('agreements/companies/pdf/{id}', 'AgreementsCompaniesController@agreementPdf');
Route::get('balance_account/{type}/{id}', 'AgreementsController@balance_account');

// Customers
Route::resource('customers', 'CustomersController');
Route::post('customers/updatecontact', 'CustomersController@updateContact');
Route::get('customers/info/{id}', 'CustomersController@info');
Route::post('customers/autocomplete', 'CustomersController@autocomplete');
Route::post('customers/getcustomer', 'CustomersController@getCustomer');
Route::get('customers/infocontact/{id}', 'CustomersController@infoContact');
Route::get('customers/destroycontact/{id}', 'CustomersController@destroyContact');
Route::post("customers/formatidentity","CustomersController@getFormatidentity");
Route::post("customers/addcontact/{id}","CustomersController@addContact");


// Companies
Route::resource('companies', 'CompaniesController');
Route::post("companies/autocomplete","CompaniesController@autocomplete");
Route::post("companies/formatidentity","CompaniesController@getFormatidentity");
Route::post("companies/getcombobox","CompaniesController@getComboBox");
Route::post("companies/searchsecuritycompanies","CompaniesController@searchSecurityCompanies");

// Accountstatus
Route::resource('accountstatus', 'AccountStatusController');
Route::get('customer/accountstatus/{id}', 'AccountStatusController@customer');
Route::get('company/accountstatus/{id}', 'AccountStatusController@company');

// Agreements
Route::resource('agreementscompanies', 'AgreementsCompaniesController');
Route::resource('agreementscustomers', 'AgreementsController');
Route::get('agreements/getdata/{id}', 'AgreementsCompaniesController@getData');
Route::post('agreements/getagreements', 'AgreementsCompaniesController@getAgreements');
Route::get('agreements/customers/pdf/{id}', 'AgreementsController@agreementPdf');
Route::get('agreements/companies/pdf/{id}', 'AgreementsCompaniesController@agreementPdf');
Route::get('balance_account/{type}/{id}', 'AgreementsController@balance_account');

// Services
Route::resource('services', 'ServicesController');
Route::post('services/removeitems', 'ServicesController@destroyItems');
Route::post('services/getservices', 'ServicesController@getServices');

// Emails
Route::get('emails/inspection', 'EmailsController@inspection');