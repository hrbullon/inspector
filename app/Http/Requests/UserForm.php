<?php

namespace App\Http\Requests;


use Crypt;
use App\User;
use App\Http\Requests\Request;


class UserForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("users");
        return [
            'c_identity'=> (!is_null($id))?  'required|unique:users,c_identity,'. User::find(Crypt::decrypt($id))->id : "required|unique:users",
            'username'=> (!is_null($id))?  'required|unique:users,username,'. User::find(Crypt::decrypt($id))->id: "required|unique:users",
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=> (!is_null($id))?  'required|unique:users,email,'. User::find(Crypt::decrypt($id))->id : "required|unique:users",
            'type_identity'=>'required|int|min:1',
        ];
    }
}
