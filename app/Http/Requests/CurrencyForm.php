<?php

namespace App\Http\Requests;

use Crypt;
use App\Currency;
use App\Http\Requests\Request;

class CurrencyForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("currency");
        return [
            'country'=>'required',
            'name'=> (!is_null($id))?  'required|unique:currency,name,'. Currency::find(Crypt::decrypt($id))->id : "required|unique:currency",
            'plural'=>'required|max:50|min:1',
            'abbreviated'=>'required|max:5|min:1',
            'exchange_purchase'=>'required|max:12',
            'exchange_sale'=>'required|max:12',
        ];
    }
}
