<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgreementsCustomersForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer'=>'required|int',
            'date'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'address'=>'required',
            'mstatus'=>'required|int'
        ];
    }
}
