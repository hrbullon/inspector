<?php

namespace App\Http\Requests;

use Crypt;
use App\CustomersContact;
use App\Http\Requests\Request;

class CustomersContactForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("id");
        return [
            'customer'=>'required',
            'contact'=> (!is_null($id))? 'required|unique:customers_contact,contact,'. CustomersContact::find(Crypt::decrypt($id))->id : "required|unique:customers_contact",
            'type'=>'required|int',
            'mstatus'=>'required|int',
        ];
    }
}
