<?php

namespace App\Http\Requests;

use Crypt;
use App\BankAccounts;
use App\Http\Requests\Request;

class BankAccountsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("id");
        $belong = $this->input("belong");

        return [
            'account'=> (!is_null($id))?  'required|unique:bank_accounts,account,'. BankAccounts::find(Crypt::decrypt($id))->id : "required|unique:bank_accounts",
            'bank'=>'required|int',
            'belong'=> 'required|int',
            'company'=> ($belong == 1)?  'required|int': '',
            'type'=>'required|int',
            'm_status'=>'required|int',
        ];
    }
}
