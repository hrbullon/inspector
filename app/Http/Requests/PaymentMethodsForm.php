<?php

namespace App\Http\Requests;

use Crypt;
use App\Http\Requests\Request;
use App\PaymentMethods;

class PaymentMethodsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("id");

        return [
            'name'=> (!is_null($id))?  'required|unique:payment_methods,name,'. PaymentMethods::find(Crypt::decrypt($id))->id : "required|unique:payment_methods",
            'date_mandatory'=>'required|int',
            'reference_mandatory'=>'required|int',
            'bank_mandatory'=>'required|int',
            'ps_mandatory'=>'required|int',
            'lot_mandatory'=>'required|int',
            'conformation_mandatory'=>'required|int',
            'type_tax_mandatory'=>'required|int'
        ];
    }
}
