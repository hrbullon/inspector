<?php

namespace App\Http\Requests;

use Crypt;
use App\Http\Requests\Request;
use App\Taxes;

class TaxesForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("taxes");
        return [
            'country'=>'required',
            'name'=> (!is_null($id))?  'required|unique:taxes,name,'. Taxes::find(Crypt::decrypt($id))->id : "required|unique:taxes",
            'aliquot'=>'required|max:13',
            'applicable_from'=>'required|date'
        ];
    }
}
