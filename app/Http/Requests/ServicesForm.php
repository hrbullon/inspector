<?php

namespace App\Http\Requests;

use Crypt;
use App\Http\Requests\Request;
use App\Services;

class ServicesForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("services");

        return [
            'inning'=>'required',
            'name'=> (!is_null($id))?  'required|unique:services,name,'. Services::find(Crypt::decrypt($id))->id : "required|unique:services",
            'number_visit'=>'required',
            'days'=>'required',
            'hour_from'=>'required',
            'hour_to'=>'required',
            'amount'=>'required',
            'mstatus'=>'required',
        ];
    }
}
