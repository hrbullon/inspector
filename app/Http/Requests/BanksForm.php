<?php

namespace App\Http\Requests;

use App\Banks;
use Crypt;
use App\Http\Requests\Request;

class BanksForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("id");

        return [
            'name'=> (!is_null($id))?  'required|unique:banks,name,'. Banks::find(Crypt::decrypt($id))->id : "required|unique:banks",
            'country'=>'required|int',
        ];
    }
}
