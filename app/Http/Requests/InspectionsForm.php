<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InspectionsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type =  $this->input('type');

        return [
            'customer'=>'required',
            'type'=>'required',
            'date_created'=>'required',
            'planning'=> (!is_null($type) && $type == 78 )?  'required' : '',
            'activation'=> (!is_null($type) && $type == 79 )?  'required' : '',
            'alarm_count'=> (!is_null($type) && $type == 80 )?  'required' : '',
            'latitude'=> (!is_null($type) && $type == 78 )?  'required' : '',
            'longitude'=> (!is_null($type) && $type == 78)?  'required' : '',
        ];
    }
}
