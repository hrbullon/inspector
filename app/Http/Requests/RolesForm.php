<?php

namespace App\Http\Requests;

use Crypt;
use App\Roles;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Route;

class RolesForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("roles");
        return [
            'name'=> (!is_null($id))?  'required|unique:roles,name,'. Roles::find(Crypt::decrypt($id))->id : "required|unique:roles",
            'slug'=> (!is_null($id))?  'required|unique:roles,slug,'. Roles::find(Crypt::decrypt($id))->id : "required|unique:roles",
            'description'=>'required',
        ];
    }
}
