<?php

namespace App\Http\Requests;

use Crypt;
use App\AlarmCount;
use App\Http\Requests\Request;

class AlarmCountForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("alarmcount");
        return [
            'customer'=>'required|int',
            'protocol'=>'required|int',
            'account'=> (!is_null($id))?  'required|unique:alarm_count,account,'. AlarmCount::find(Crypt::decrypt($id))->id : "required|unique:alarm_count,account",
            'name'=>'required',
            'country'=>'required',
            'states'=>'required',
            'town'=>'required',
            'parish'=>'required',
            'address'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'mstatus'=>'required|int',
        ];
    }
}
