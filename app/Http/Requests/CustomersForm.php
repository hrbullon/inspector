<?php

namespace App\Http\Requests;

use Crypt;
use App\Customers;
use App\Http\Requests\Request;

class CustomersForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("customers");
        return [
            'type'=>'required',
            'c_identity'=> (!is_null($id))?  'required|unique:customers,c_identity,'. Customers::find(Crypt::decrypt($id))->id : "required|unique:customers",
            'first_name'=>'required|regex:/^[a-zA-Z]+$/',
            'second_name'=>'regex:/^[a-zA-Z]+$/',
            'first_surname'=>'regex:/^[a-zA-Z]+$/',
            'second_surname'=>'regex:/^[a-zA-Z]+$/',
            'address'=>'required',
            'mstatus'=>'required',
            'ispotential'=>'required',
        ];
    }
}
