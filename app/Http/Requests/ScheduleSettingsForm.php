<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ScheduleSettingsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "weekly_hours"=> "required",
            "dailyh_min"=> "required",
            "dailyh_max"=> "required",
            "handles_morning"=> "required",
            "morning_beggining"=> ($this->request->get("handles_morning"))? "required" : "",
            "morning_finish"=> ($this->request->get("handles_morning"))? "required" : "",
            "handles_evening"=> "required",
            "evening_beggining"=> ($this->request->get("handles_evening"))? "required" : "",
            "evening_finish"=> ($this->request->get("handles_evening"))? "required" : "",
            "handles_night"=> "required",
            "night_beggining"=> ($this->request->get("handles_night"))? "required" : "",
            "night_finish"=> ($this->request->get("handles_night"))? "required" : "",
            "number_days"=> "required",
            "hours_daily"=> "required"
        ];
    }


}
