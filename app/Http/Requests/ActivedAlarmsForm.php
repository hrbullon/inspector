<?php

namespace App\Http\Requests;

use Crypt;
use App\Http\Requests\Request;

class ActivedAlarmsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activation_date'=>'required',
            'account'=>'required',
        ];
    }
}
