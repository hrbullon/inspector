<?php

namespace App\Http\Requests;

use Crypt;
use App\Http\Requests\Request;
use App\Permissions;

class PermissionsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("permissions");
        return [
            'name'=> (!is_null($id))?  'required|unique:permissions,name,'. Permissions::find(Crypt::decrypt($id))->id : "required|unique:permissions",
            'slug'=> (!is_null($id))?  'required|unique:permissions,slug,'. Permissions::find(Crypt::decrypt($id))->id : "required|unique:permissions",
            'description'=>'required',
        ];
    }
}
