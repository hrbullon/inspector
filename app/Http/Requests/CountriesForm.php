<?php

namespace App\Http\Requests;

use Crypt;
use App\Countries;
use App\Http\Requests\Request;

class CountriesForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("countries");
        return [
            'name'=> (!is_null($id))?  'required|unique:countries,name,'. Countries::find(Crypt::decrypt($id))->id : "required|unique:countries",
            'gtm'=>'required|integer|min:1',
            'latitude'=>'regex:/([0-9.-]+).+?([0-9.-]+)/',
            'longitude'=>'regex:/([0-9.-]+).+?([0-9.-]+)/',
        ];
    }
}
