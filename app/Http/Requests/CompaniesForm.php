<?php

namespace App\Http\Requests;

use Crypt;
use App\Companies;
use App\Http\Requests\Request;

class CompaniesForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter("companies");
        $rules = [
            'c_identity'=> (!is_null($id))?  'required|unique:companies,c_identity,'. Companies::find(Crypt::decrypt($id))->id : "required|unique:companies",
            'name'=> (!is_null($id))?  'required|unique:companies,name,'. Companies::find(Crypt::decrypt($id))->id : "required|unique:companies",
            'logotype'=> (!is_null($id))?  'max:50' : "required",
            'c_identity'=> ['required', 'regex:/^[G|J|V]([0-9]{7,11})$/'],
            'alias'=>'required|max:50',
            'address'=>'required',
            'type'=>'required|integer',
            'handles_credit'=>'required|integer',
            'credit_days'=>'required|integer',
            'credit_limit'=>'required|max:12',
            'type_taxpayer'=>'required|integer',
            'country'=>'required|integer',
            'state'=>'required|integer',
            'town'=>'required|integer',
            'parish'=>'required|integer',
            'city'=>'required',
            'zip'=>'required|integer',
            'latitude'=>'required|regex:/([0-9.-]+).+?([0-9.-]+)/',
            'longitude'=>'required|regex:/([0-9.-]+).+?([0-9.-]+)/',
            'm_status'=>'required|integer'
        ];
        return $rules;
    }
}
