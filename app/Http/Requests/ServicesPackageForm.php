<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicesPackageForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country'=>'required',
            'type'=>'required',
            'quantity'=>'integer',
            'amount'=>'required|numeric',
            'frecuency'=>'required|integer',
            'tope_n_customers'=>'integer',
            'm_status'=>'required|integer',
            'mandatory'=>'required|integer',
        ];
    }
}
