<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsSecurityCompany extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_security_company';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public static function insertSecurityCompanies($agreement,$companies)
    {

        for($x = 0; $x < count($companies); $x++)
        {
            $security = new AgreementsSecurityCompany();
            $security->agreements = $agreement;
            $security->security_company = $companies;
            $security->save();
        }
    }
}
