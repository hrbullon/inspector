<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'states';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country', 'name'];

    public function scopeSearch($query, $search)
    {
        $query->select("states.id","countries.name as country","states.name");
        $query->join('countries', 'countries.id', '=', 'states.country');

        if($search[0] != ""){
            $query->where("states.name","LIKE","%".$search[0]."%");
        }
        if($search[1] != null)
        {
            $query->where("states.country",$search[1]);
        }

    }
}
