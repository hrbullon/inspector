<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivedAlarmsZonning extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actived_alarms_zoning';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['activation', 'zoning'];

    public static function GetZoning($activation)
    {
        return \DB::table('actived_alarms_zoning')
            ->join('zoning_alarm', 'zoning_alarm.id', '=', 'actived_alarms_zoning.zoning')
            ->select('zoning_alarm.id',  'zoning_alarm.name','zoning_alarm.mstatus')
            ->where('actived_alarms_zoning.activation',$activation)->get();
    }

    public static function InsertZoning($activation,$request)
    {
        for($x=0; $x < count($request['zonas']); $x++)
        {
            $activedZone = new ActivedAlarmsZonning();
            $activedZone->activation = $activation->id;
            $activedZone->zoning = $request['zonas'][$x]['zona'];
            $activedZone->save();
        }
    }
}
