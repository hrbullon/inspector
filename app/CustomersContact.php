<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersContact extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers_contact';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['customer','type','contact','person','mstatus'];

    public static function insertContact($customer, $contact)
    {
        for ($x = 0; $x < count($contact); $x++) {
            $customer_contact = new CustomersContact();
            $customer_contact->customer = $customer;
            $customer_contact->type = $contact[$x]['type'];
            $customer_contact->contact = $contact[$x]['contact'];
            $customer_contact->person = $contact[$x]['person'];
            $customer_contact->mstatus = $contact[$x]['mstatus'];
            $customer_contact->save();
        }
    }
}
