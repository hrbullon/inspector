<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Roles extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *Hola hadelson como estas soy yo tio el gran paco no me conoces pues me conoces, ya tenemos cena tio!
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'level'];

    public function scopeSearch($query, $search)
    {
        if($search[0] != null)
        {
            $query->where("name","LIKE","%".$search[0]."%");
            $query->orWhere("slug","LIKE","%".$search[0]."%");
            $query->orWhere("description","LIKE","%".$search[0]."%");
        }
        if(!User::isAdmin())
        {
            $query->orWhere("company",Session::get("company"));
        }
    }

    /**
     * Get ROles for User loggueado  .
     */
    public static function getRoles()
    {
        return RoleUser::where("user",Session::get("user"))->get();
    }
}
