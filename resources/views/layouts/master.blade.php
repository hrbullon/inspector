<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="inspector-web">
    <meta name="Saga-Software" content="Inspector-web">
    <meta name="_token"  content="{{ Session::token() }}">
    <link rel="shortcut icon" href="assets/global/images/favicon.png" type="image/png">
    <title>Saga - Inspector | Web</title>
	<!-- BEGIN PLUGINS STYLES -->
		<link href="assets/global/css/style.css" rel="stylesheet">
        <link href="assets/global/css/theme.css" rel="stylesheet">
        <link href="assets/global/css/ui.css" rel="stylesheet">
        <link href="assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
        <link href="assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
        <link href="assets/global/plugins/slick/slick.css" rel="stylesheet">
        <link href="assets/global/plugins/icheck/skins/all.css" rel="stylesheet">
        <link href="assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
        <link href="assets/global/plugins/rateit/rateit.css" rel="stylesheet" />
        <link href="assets/global/plugins/colorpicker/spectrum.css" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/css/step-form-wizard.css" rel="stylesheet" />
        <link href="assets/global/plugins/step-form-wizard/plugins/parsley/parsley.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/ion.rangeSlider.css" rel="stylesheet" />
        <link href="assets/global/plugins/ion-slider/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/cke-editor/skins/bootstrapck/editor.css" rel="stylesheet" />
        <link href="assets/global/plugins/summernote/summernote.css" rel="stylesheet" />
        <link href="assets/global/plugins/cropper/cropper.css" rel="stylesheet" />
        <link href="assets/global/plugins/magnific/magnific-popup.css" rel="stylesheet" />
        <link href="assets/global/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
        <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/prettify/prettify.css" rel="stylesheet" />
        <link href="assets/global/plugins/jstree/dist/themes/default/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/jstree/dist/themes/default/style.css" rel="stylesheet" />
        <link href="assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/input-text/style.min.css" rel="stylesheet" />
        <link href="assets/global/plugins/font-awesome-animation/font-awesome-animation.min.css" rel="stylesheet" />
	<!-- END PLUGINS STYLES -->
    <!-- BEGIN ANGULARJS STYLE -->
	    <link href="md-layout/angularjs/css/angular-theme.css" rel="stylesheet">
	<!-- END ANGULARJS STYLE -->
	<!-- BEGIN LAYOUT STYLE-->
		<link href="assets/admin/md-layout/material-design/css/material.css" rel="stylesheet">
        <link href="assets/admin/md-layout/css/layout.css" rel="stylesheet">
	<!-- END LAYOUT STYLE-->
	<!-- MODERNIZR -->
        <script src="assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body ng-app="newApp" class="fixed-topbar fixed-sidebar theme-sdtd color-blue" ng-controller="mainCtrl">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- BEGIN PAGE SPINNER -->
<div ng-spinner-loader class="page-spinner-loader">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
</div>
<!-- END PAGE SPINNER -->

<section>

    <!-- BEGIN SIDEBAR -->
    <div class="sidebar">
        <div class="logopanel center" style="padding: 5px !important;">
            <a href="#">
                <img src="assets/global/images/logo/inspector_logo.png" width="180px" height="42px">
            </a>
        </div>
        <div class="sidebar-inner">
            <div class="menu-title">
                Navegaci&oacute;n
            </div>
            <ul class="nav nav-sidebar" ng-click="changeCompany()">
                {{ \App\Views::getPanelItems()  }}
            </ul>
            <div class="sidebar-footer clearfix" style="background-color: white">
                <a class="pull-left footer-settings" data-target="#" data-rel="tooltip" data-placement="top" data-original-title="Opciones">
                    <i class="icon-settings"></i>
                </a>
                <a class="pull-left toggle_fullscreen" data-target="#" data-rel="tooltip" data-placement="top" data-original-title="Pantalla Completa">
                    <i class="icon-size-fullscreen"></i>
                </a>
                <a class="pull-left" href="lockscreen" data-rel="tooltip" data-placement="top" data-original-title="Bloquear Pantalla">
                    <i class="icon-lock"></i>
                </a>
                <a class="pull-left btn-effect" href="logout" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Cerrar Sesi&oacute;n">
                    <i class="icon-power"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
                </div>

            </div>
            <div class="header-right">
                <ul class="header-menu nav navbar-nav">
                    <!-- BEGIN USER DROPDOWN -->
                    <li class="dropdown" id="user-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="assets/global/images/avatars/{!! Auth::user()->avatar !!}" alt="user image">
                            <span class="username">Hola, {!! Auth::user()->firstname !!}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-target="#"><i class="icon-user"></i><span>Mi Perfil</span></a>
                            </li>
                            <li>
                                <a data-target="#"><i class="icon-settings"></i><span>Opciones</span></a>
                            </li>
                            <li>
                                <a href="logout"><i class="icon-logout"></i><span>Cerrar Sesi&oacute;n</span></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div ng-view class="at-view-slide-in-left page-content" ng-view-class=" page-wizard : /forms-wizard , page-thin : / , page-contact : /pages-contact,
                    page-profil : /user-profile, page-app : /user-profile" >
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END MAIN CONTENT -->
</section>
<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!--- BEGIN GLOBAL MODAL --->
<div class="modal fade modal-slideleft" id="modal-slideleft" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="c-primary m-b-30">Eliminar ?</h2>
                        <button type="button" id="yes" class="btn btn-primary btn-embossed btn-block" data-dismiss="modal">Si</button>
                        <button type="button" id="not" data-dismiss="modal" class="btn btn-white btn-block">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- JQUERY LIBRARIES-->
<script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<!-- BEGIN ANGULARJS SCRIPTS -->
<script src="md-layout/angularjs/plugins/angular/angular.js"></script>
<script src="md-layout/angularjs/plugins/json3/lib/json3.js"></script>

<script src="assets/global/plugins/select2/select2.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.js"></script>
<script src="http://rawgit.com/angular-ui/angular-google-maps/2.0.X/dist/angular-google-maps.js"></script>
<script src="md-layout/angularjs/plugins/angular-simple-logger/dist/angular-simple-logger.js"></script>


<script src="md-layout/angularjs/plugins/angular-resource/angular-resource.js"></script>
<script src="md-layout/angularjs/plugins/angular-messages/angular-messages.js"></script>
<script src="md-layout/angularjs/plugins/angular-cookies/angular-cookies.js"></script>
<script src="md-layout/angularjs/plugins/angular-sanitize/angular-sanitize.js"></script>
<script src="md-layout/angularjs/plugins/angular-animate/angular-animate.js"></script>
<script src="md-layout/angularjs/plugins/angular-touch/angular-touch.js"></script>
<script src="md-layout/angularjs/plugins/angular-route/angular-route.js"></script>
<script src="md-layout/angularjs/plugins/angular-ui-select2/src/select2.js"></script>
<script src="md-layout/angularjs/plugins/angular-bootstrap/ui-bootstrap-tpls-0.12.1.js"></script>
<script src="md-layout/angularjs/plugins/ng-table/dist/ng-table.js"></script>
<script src="md-layout/angularjs/app/app.js"></script>
<script src="md-layout/angularjs/app/mainCtrl.js"></script>
<script src="md-layout/angularjs/directives/ngViewClass.js"></script>
<!-- END ANGULARJS SCRIPTS -->
<!-- BEGIN PLUGINS -->
<script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
<script src="assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>
<script src="assets/global/plugins/bootbox/bootbox.min.js"></script>
<script src="assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="assets/global/plugins/charts-sparkline/sparkline.min.js"></script>
<script src="assets/global/plugins/retina/retina.min.js"></script>
<script src="assets/global/plugins/backstretch/backstretch.min.js"></script>
<script src="assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="assets/global/js/sidebar_hover.js"></script>
<script src="assets/global/js/widgets/notes.js"></script>
<script src="assets/global/js/pages/search.js"></script>
<script src="assets/global/plugins/quickSearch/quicksearch.js"></script>
<script src="assets/global/plugins/slick/slick.js"></script>
<script src="assets/global/plugins/icheck/icheck.js"></script>
<script src="assets/global/plugins/switchery/switchery.js"></script>
<script src="assets/global/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/global/plugins/colorpicker/spectrum.js"></script>
<script src="assets/global/plugins/touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/global/plugins/step-form-wizard/js/step-form-wizard.js"></script>
<script src="assets/global/plugins/step-form-wizard/plugins/parsley/parsley.min.js"></script>
<script src="assets/global/plugins/jquery-validation/jquery.validate.js"></script>
<script src="assets/global/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script src="assets/global/plugins/ion-slider/ion.rangeSlider.js"></script>
<script src="assets/global/plugins/bootstrap/js/jasny-bootstrap.js"></script>
<script src="assets/global/plugins/isotope/isotope.pkgd.min.js"></script>
<script src="assets/global/plugins/magnific/jquery.magnific-popup.js"></script>
<script src="assets/global/plugins/moment/moment.min.js"></script>
<script src="assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="assets/global/plugins/countup/countUp.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script src="assets/global/plugins/google-maps/gmaps.js"></script>
<script src="assets/global/plugins/google-maps/markerclusterer.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/ammap_amcharts_extension.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/maps/js/continentsLow.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.js"></script>
<script src="assets/global/plugins/maps-amcharts/ammap/maps/js/usaLow.js"></script>
<script src="assets/global/plugins/bootstrap-loading/lada.min.js"></script>
<script src="assets/global/plugins/jstree/jstree.js"></script>
<script src="assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/global/plugins/typed/typed.js"></script>
<script src="assets/global/plugins/cke-editor/ckeditor.js"></script>
<script src="assets/global/plugins/cke-editor/config.js"></script>
<script src="assets/global/plugins/cke-editor/styles.js"></script>
<script src="assets/global/plugins/cke-editor/adapters/adapters.min.js"></script>
<script src="assets/global/plugins/cke-editor/lang/en.js"></script>
<script src="assets/global/plugins/cke-editor/skins/bootstrapck/skin.js"></script>
<script src="assets/global/plugins/summernote/summernote.js"></script>
<script src="assets/global/plugins/prettify/prettify.js"></script>
<script src="assets/global/plugins/dropzone/dropzone.min.js"></script>
<script src="assets/global/plugins/idle-timeout/jquery.idletimeout.min.js"></script>
<script src="assets/global/plugins/idle-timeout/jquery.idletimer.min.js"></script>
<script src="assets/global/plugins/cropper/cropper.js"></script>
<script src="assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>
<script src="assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<script src="assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
<script src="assets/global/plugins/multidatepicker/multidatespicker.min.js"></script>
<script src="assets/global/js/widgets/todo_list.js"></script>
<script src="assets/global/plugins/metrojs/metrojs.min.js"></script>
<script src="assets/global/plugins/charts-chartjs/Chart.min.js"></script>
<script src="assets/global/plugins/charts-highstock/js/highstock.min.js"></script>
<script src="assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script>
<script src="assets/global/plugins/skycons/skycons.min.js"></script>
<script src="assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script>
<script src="assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.js"></script>
<script src="assets/global/plugins/rateit/jquery.rateit.min.js"></script>
<script src="assets/global/plugins/charts-highstock/js/highcharts-more.min.js"></script>
<script src="assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script>
<script src="assets/global/plugins/autosize/autosize.min.js"></script>
<script src="assets/global/js/widgets/widget_weather.js"></script>
<script src="assets/global/js/pages/ecommerce.js"></script>
<script src="assets/global/plugins/velocity/velocity.min.js"></script>
<script src="md-layout/angularjs/plugins/angular-autocomplete-places/ngAutocomplete.js"></script>
<script src="md-layout/angularjs/js/pages/dashboard.js"></script>
<script src="md-layout/angularjs/js/pages/charts.js"></script>
<script src="md-layout/angularjs/js/pages/charts_finance.js"></script>
<script src="md-layout/angularjs/js/pages/layouts_api.js"></script>
<script src="md-layout/angularjs/js/builder.js"></script>
<script src="md-layout/angularjs/js/application.js"></script>
<script src="md-layout/angularjs/js/plugins.js"></script>
<script src="md-layout/angularjs/js/quickview.js"></script>
<!-- END PLUGINS -->
<script src="assets/admin/md-layout/material-design/js/material.js"></script>
<!-- BEGIN SCRIPTS VIEWS -->
<script src="md-layout/angularjs/app/dashboard/dashboardCtrl.js"></script>
        <script src="md-layout/angularjs/app/charts/charts/chartsCtrl.js"></script>
        <script src="md-layout/angularjs/app/charts/financialCharts/financialChartsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/Tabs/tabsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/buttons/buttonsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/colors/colorsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/modals/modalsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/components/componentsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/animations/animationsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/icons/iconsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/nestableList/nestableListCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/treeView/treeViewCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/notifications/notificationsCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/typography/typographyCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/helperClasses/helperClassesCtrl.js"></script>
        <script src="md-layout/angularjs/app/mailbox/mailboxTemplatesCtrl.js"></script>
        <script src="md-layout/angularjs/app/mailbox/mailboxCtrl.js"></script>
        <script src="md-layout/angularjs/app/mailbox/mailSendCtrl.js"></script>
        <script src="md-layout/angularjs/app/mainCtrl.js"></script>
        <script src="md-layout/angularjs/app/medias/croping/cropingCtrl.js"></script>
        <script src="md-layout/angularjs/app/medias/hover/hoverCtrl.js"></script>
        <script src="md-layout/angularjs/app/medias/sortable/sortableCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/editors/editorsCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/elements/elementsCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/inputMasks/inputMasksCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/plugins/pluginsCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/sliders/slidersCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/validation/validationCtrl.js"></script>
        <script src="md-layout/angularjs/app/forms/wizard/wizardCtrl.js"></script>
        <script src="md-layout/angularjs/app/pages/timeline/timelineCtrl.js"></script>
        <script src="md-layout/angularjs/app/pages/blank/blankCtrl.js"></script>
        <script src="md-layout/angularjs/app/pages/contact/contactCtrl.js"></script>
        <script src="md-layout/angularjs/app/extra/fullCalendar/fullCalendarCtrl.js"></script>
        <script src="md-layout/angularjs/app/extra/widgets/widgetsCtrl.js"></script>
        <script src="md-layout/angularjs/app/extra/slider/sliderCtrl.js"></script>
        <script src="md-layout/angularjs/app/extra/google/googleCtrl.js"></script>
        <script src="md-layout/angularjs/app/extra/vector/vectorCtrl.js"></script>
        <script src="md-layout/angularjs/app/ecommerce/pricingTable/pricingTableCtrl.js"></script>
        <script src="md-layout/angularjs/app/ecommerce/invoice/invoiceCtrl.js"></script>
        <script src="md-layout/angularjs/app/ecommerce/cart/cartCtrl.js"></script>
        <script src="md-layout/angularjs/app/layout/apiCtrl.js"></script>
        <script src="md-layout/angularjs/app/tables/dynamic/dynamicCtrl.js"></script>
        <script src="md-layout/angularjs/app/tables/editable/editableCtrl.js"></script>
        <script src="md-layout/angularjs/app/tables/filter/filterCtrl.js"></script>
        <script src="md-layout/angularjs/app/tables/styling/stylingCtrl.js"></script>
        <script src="md-layout/angularjs/app/user/profile/profileCtrl.js"></script>
        <script src="md-layout/angularjs/app/user/sessionTimeout/sessionTimeoutCtrl.js"></script>
        <script src="md-layout/angularjs/app/uiElements/cards/cardsCtrl.js"></script>

		<script src="md-layout/angularjs/app/roles/rolesCtrl.js"></script>
     	<script src="md-layout/angularjs/app/agreements/customers/agreementscustomersCtrl.js"></script>

<!-- END SCRIPTS VIEWS -->
        <script src="assets/admin/md-layout/material-design/js/material.js"></script>
</body>
</html>