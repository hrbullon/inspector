<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Saga-Inspector - Autorización</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="saga-inspector login" />
    <meta content="themes-lab" name="saga-inspector" />
    <link rel="shortcut icon" href="assets/global/images/favicon.png">
    <link href="assets/global/css/style.css" rel="stylesheet">
    <link href="assets/global/css/ui.css" rel="stylesheet">
    <link href="assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
</head>
<body class="account2 no-social" data-page="login">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="login-block">
    <i class="user-img icons-faces-users-03"></i>
    <div class="account-info">
        <img src="assets/global/images/logo/inspector_logo.png" width="160px">
        <h3>Panel de Control</h3>
        <ul>
            <li><i class="icon-folder-alt"></i> Gestión de Contratos</li>
            <li><i class="icon-calendar"></i> Planificación de Horarios</li>
            <li><i class="icon-credit-card"></i>  Registro de Pagos</li>
            <li><i class="icon-bar-chart"></i> Estadísticas</li>
        </ul>
    </div>
    <div class="account-form">
        <form class="form-signin" role="form" method="post" action="login">
            <h3><strong>INICIE SESIÓN</strong> CON SU CUENTA</h3>
            {!! csrf_field() !!}
            <div class="append-icon">
                <input type="text" name="email" id="email" class="form-control form-white username" placeholder="email" required>
                <i class="icon-user"></i>
            </div>
            <div class="append-icon m-b-20">
                <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                <i class="icon-lock"></i>
            </div>
            <button type="submit" class="btn btn-lg btn-dark btn-rounded ladda-button" data-style="expand-left">Entrar</button>
            <span class="forgot-password">
                <a id="password" href="account-forgot-password.html">¿Olvidó su clave?</a></span>
            <div class="form-footer">
                <div class="clearfix">
                    <p class="new-here"><a href="user-signup-v2.html">¿Nuevo Usuario? Registrar</a></p>
                </div>
            </div>
        </form>
        <form class="form-password" role="form">
            <h3><strong>Reset</strong> su contraseña</h3>
            <div class="append-icon m-b-20">
                <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                <i class="icon-lock"></i>
            </div>
            <button type="submit" id="submit-password" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Enviar link de reseteo de contraseña</button>
            <div class="clearfix m-t-60">
                <p class="pull-left m-t-20 m-b-0"><a id="login" href="#">¿Tiene una cuenta? Entrar</a></p>
                <p class="pull-right m-t-20 m-b-0"><a href="user-signup2.html">¿Nuevo Usuario? Registrar</a></p>
            </div>
        </form>
    </div>
    <div>
        @if (count($errors) > 0)
            <div class="alert alert-warning alert-dismissible" role="alert" style="margin-top: 5%;padding: 20px 0px 20px 40px; width: 580px;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                <ul>
                    <strong>{{ trans('NOTIFICACIÓN') }}</strong>
                    <li>El usuario o contraseña no coinciden</li>
                </ul>
            </div>
        @endif
        <!--@if (count($errors) > 0)
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                    <strong>{{ trans('notifications.alert') }}</strong>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif-->

        <!--@if (count($errors) > 0)
            <div class="row">
                <div class="col-md-11 alert alert-danger" style="margin-top: 5%; padding: 20px 0px 20px 40px">
                    <h4><i class="fa fa-close"></i> {!! Session::get('error') !!}</h4>
                </div>
            </div>
            <br>
        @endif-->
    </div>
</div>
<!-- END LOCKSCREEN BOX -->
<p class="account-copyright">
    <span>Copyright © {!! date("Y") !!} </span><span><a href="http://sagasoftware.net/">SAGA Software, C.A.</a></span>.<span>Todos los derechos Reservados.</span>
</p>
<script src="assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/global/plugins/backstretch/backstretch.min.js"></script>
<script src="assets/global/plugins/bootstrap-loading/lada.min.js"></script>
<script src="assets/global/js/pages/login-v2.js"></script>
</body>
</html>